﻿// <copyright file="Control.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.WPF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using Data;
    using NinjaGame.Logic;
    using Repository;

    /// <summary>
    /// Responsible for controlling the app.
    /// </summary>
    public class Control : FrameworkElement
    {
        private GameLogic gl;
        private GameModel gm;
        private Display gd;
        private DispatcherTimer timer = new DispatcherTimer();
        private Window window;

        /// <summary>
        /// Initializes a new instance of the <see cref="Control"/> class.
        /// </summary>
        public Control()
        {
            this.Loaded += this.Control_Loaded;
        }

        /// <summary>
        /// Gets or sets the working gamemodel of the control.
        /// </summary>
        public GameModel Gm
        {
            get { return this.gm; }
            set { this.gm = value; }
        }

        /// <summary>
        /// Gets or sets the timer of the control.
        /// </summary>
        public DispatcherTimer Timer
        {
            get { return this.timer; }
            set { this.timer = value; }
        }

        /// <summary>
        /// Controls the rendering.
        /// </summary>
        /// <param name="drawingContext">A drawing context to use.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.gl == null || this.ActualWidth == 0)
            {
                return;
            }

            drawingContext.DrawDrawing(this.gd.GetDrawings());
        }

        private void Control_Loaded(object sender, RoutedEventArgs e)
        {
            this.window = Window.GetWindow(this);
            if (this.window != null)
            {
                this.Gm = new GameModel();
                this.gl = new GameLogic(this.Gm, this.ActualWidth, this.ActualHeight);
                this.gd = new Display(this.Gm, this.ActualWidth, this.ActualHeight);

                this.gl.RefreshScreen += this.Logic_ScreenRefresh;
                this.window.KeyDown += this.Window_KeyDown;

                // Gombok feliratkozás
                // Some code
                this.timer.Interval = TimeSpan.FromMilliseconds(30);
                this.timer.Tick += this.Timer_Tick;

                // this.timer.Start();
                this.gl.RefreshScreen += (obj, args) => this.InvalidateVisual();
                this.InvalidateVisual();
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.P)
            {
                if (this.gl.IsPaused == true)
                {
                    this.gl.IsPaused = !this.gl.IsPaused;
                    this.timer.Start();
                    this.gl.NinjaGameModel.LevelStopwatch.Start();
                }
                else
                {
                    this.gl.IsPaused = !this.gl.IsPaused;
                    this.timer.Stop();
                    this.gl.NinjaGameModel.LevelStopwatch.Stop();
                }
            }

            if (e.Key == Key.M)
            {
                this.gl.Save();
            }

            if (e.Key == Key.X)
            {
                this.window.Close();
            }

            if (e.Key == Key.H)
            {
                if (this.gl.IsPaused == false)
                {
                    this.gl.IsPaused = true;
                    this.gl.NinjaGameModel.LevelStopwatch.Stop();
                }

                HighScoreWindow hsw = new HighScoreWindow();
                hsw.Show();
            }

            if (e.Key == Key.B)
            {
                // Játék betöltése
                this.gl.IsPaused = true;
                this.gl.LoadGame();
            }
            else
            {
                if (this.gl.NinjaGameModel.GameFinsihed == false && this.gl.IsPaused == false && this.gl.NinjaGameModel.ActualPlayer.IsDead == false)
                {
                    switch (e.Key)
                    {
                        case Key.Left:
                            this.gl.MoveLeft();
                            break;
                        case Key.Right:
                            this.gl.MoveRight();
                            break;
                        case Key.Down:
                            if (this.gl.NinjaGameModel.ActualPlayer.DY == 0 &&
                            this.gl.PlayerOnPlatform() != null)
                            {
                                this.gl.DropDown();
                            }

                            break;
                        case Key.Up:
                            if (this.gl.NinjaGameModel.ActualPlayer.DY == 0 &&
                                this.gl.PlayerOnPlatform() != null &&
                                this.gl.JumpTicks == 0)
                            {
                                this.gl.JumpTicks = 4;
                                this.gl.Jump();
                            }

                            break;
                        case Key.Space:
                            if (DateTime.Now.Subtract(this.gm.ActualPlayer.ShootLastUse).TotalMilliseconds >= this.gm.ActualPlayer.CooldownMS)
                            {
                                this.gl.PlayerShoot();
                            }

                            break;

                            // Teszt, levelNövelés
                            case Key.A:
                                this.gl.IncreaseLevel();
                                break;
                    }
                }
            }
        }

        private void Logic_ScreenRefresh(object sender, EventArgs e)
        {
            this.InvalidateVisual();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (this.gl.NinjaGameModel.GameFinsihed == false && this.gl.IsPaused == false && this.gl.NinjaGameModel.ActualPlayer.IsDead == false)
            {
                this.gl.MovePlatforms();
                this.gl.MoveProjectiles();
                this.gl.Shoot();

                if (this.gl.NinjaGameModel.CurrentEnemies.OfType<Daimyo>().Any() == true)
                {
                    this.gl.MoveDaimyo();
                }

                // Nincs platformon
                if (this.gl.NinjaGameModel.ActualPlayer.Area.Top >= 0 && this.gl.PlayerOnPlatform() == null)
                {
                    // ugrás közben
                    if (this.gl.JumpTicks > 0)
                    {
                        this.gl.Jump();
                    }
                    else
                    {
                        this.gl.FallPlayer();
                        if (this.gl.NinjaGameModel.ActualPlayer.Area.Bottom >= this.ActualHeight)
                        {
                            this.gl.NinjaGameModel.ActualPlayer.IsDead = true;
                        }
                    }
                }
                else if (this.gl.NinjaGameModel.ActualPlayer.Area.Top <= 0)
                {
                    this.gl.FallPlayer();
                }

                // Platformra ér
                else if (this.gl.PlayerOnPlatform() != null &&
                    this.gl.NinjaGameModel.ActualPlayer.DY == 1)
                {
                    this.gl.StopPlayerFall();
                }

                // Platformon
                else if (this.gl.PlayerOnPlatform() != null &&
                    this.gl.NinjaGameModel.ActualPlayer.DY == 0)
                {
                    this.gl.SlidePlayer(this.gl.PlayerOnPlatform().DX, this.gl.PlayerOnPlatform().Step);
                }
            }
        }
    }
}