﻿// <copyright file="Display.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using NinjaGame.Data;
    using NinjaGame.Logic;
    using Repository;

    /// <summary>
    /// Responsible for displaying.
    /// </summary>
    public class Display
    {
        private GameModel gm;
        private double width;
        private double height;

        private ImageBrush backgroundBrush;
        private ImageBrush playerBrush;
        private ImageBrush flippedPlayerBrush;
        private ImageBrush archerBrush;
        private ImageBrush samuraiBrush;
        private ImageBrush daimyoBrush;
        private ImageBrush platformBrush;
        private ImageBrush swordBrush;
        private ImageBrush arrowBrush;
        private ImageBrush shurikenBrush;

        // healthPUBrush, shieldPUBrush

        /// <summary>
        /// Initializes a new instance of the <see cref="Display"/> class.
        /// </summary>
        /// <param name="model">The game model to use.</param>
        /// <param name="width">The width of the display.</param>
        /// <param name="height">The height of the display.</param>
        public Display(GameModel model, double width, double height)
        {
            this.gm = model;
            this.width = width;
            this.height = height;

            // Get brushes
            this.backgroundBrush = this.GetBrush("_backgroundLvl" + this.gm.CurrentLevel.Last().ToString() + ".bmp");

            this.platformBrush = this.GetBrush("_platform.bmp");

            this.playerBrush = this.GetBrush("_player.bmp");
            this.archerBrush = this.GetBrush("_archer.bmp");
            this.samuraiBrush = this.GetBrush("_samurai.bmp");
            this.daimyoBrush = this.GetBrush("_daimyo.bmp");

            this.shurikenBrush = this.GetBrush("_shuriken.bmp");
            this.arrowBrush = this.GetBrush("_arrow.bmp");
            this.swordBrush = this.GetBrush("_katana.bmp");
        }

        /// <summary>
        /// Get the image, drawings to display.
        /// </summary>
        /// <returns>A drawning in full size.</returns>
        public Drawing GetDrawings()
        {
            DrawingGroup group = new DrawingGroup();

            group.Children.Add(this.GetBackground());
            group.Children.Add(this.GetPlatforms());
            group.Children.Add(this.GetPlayer());

            if (this.gm.CurrentEnemies.OfType<Archer>().Any() == true)
            {
                foreach (var actArcher in this.gm.CurrentEnemies.OfType<Archer>())
                {
                    group.Children.Add(this.GetArcher(actArcher));
                }
            }

            if (this.gm.CurrentEnemies.OfType<Samurai>().Any() == true)
            {
                foreach (var actSamurai in this.gm.CurrentEnemies.OfType<Samurai>())
                {
                    group.Children.Add(this.GetSamurai(actSamurai));
                }
            }

            if (this.gm.CurrentEnemies.OfType<Samurai>().Any() != true && this.gm.CurrentEnemies.OfType<Archer>().Any() != true)
            {
                if (this.gm.CurrentEnemies.OfType<Daimyo>().Any() == true)
                {
                    group.Children.Add(this.GetDaimyo(this.gm.CurrentEnemies.First() as Daimyo));
                }
            }

            if (this.gm.Projectiles.OfType<Shuriken>().Any() == true)
            {
                foreach (var actShrk in this.gm.Projectiles.OfType<Shuriken>())
                {
                    group.Children.Add(this.GetShuriken(actShrk));
                }
            }

            if (this.gm.Projectiles.OfType<Arrow>().Any() == true)
            {
                foreach (var actArw in this.gm.Projectiles.OfType<Arrow>())
                {
                    group.Children.Add(this.GetArrow(actArw));
                }
            }

            if (this.gm.Projectiles.OfType<Sword>().Any() == true)
            {
                foreach (var actSwrd in this.gm.Projectiles.OfType<Sword>())
                {
                    group.Children.Add(this.GetSword(actSwrd));
                }
            }

            // group.Children.Add(GetWalls());
            // group.Children.Add(GetExit());
            // group.Children.Add(GetPlayer());
            return group;
        }

        private Drawing GetPlatforms()
        {
            GeometryGroup g = new GeometryGroup();
            foreach (var actPlatf in this.gm.Platforms)
            {
                RectangleGeometry rg = new RectangleGeometry(actPlatf.Area);
                g.Children.Add(rg);
            }

            return new GeometryDrawing(this.platformBrush, null, g);
        }

        private Drawing GetPlayer()
        {
            Geometry g = new RectangleGeometry(this.gm.ActualPlayer.Area);
            if (this.gm.ActualPlayer.DX == 1)
            {
                return new GeometryDrawing(this.playerBrush, null, g);
            }
            else
            {
                if (this.flippedPlayerBrush == null)
                {
                    this.flippedPlayerBrush = new ImageBrush(new BitmapImage(new Uri(@"..\..\_playerFlip.bmp", UriKind.Relative)));

                    return new GeometryDrawing(this.flippedPlayerBrush, null, g);
                }

                return new GeometryDrawing(this.flippedPlayerBrush, null, g);
            }
        }

        // Enemies--------------------------------------------------------------------------------------------------------------------------------
        private Drawing GetArcher(Archer archer)
        {
            Geometry g = new RectangleGeometry(archer.Area);
            return new GeometryDrawing(this.archerBrush, null, g);
        }

        private Drawing GetSamurai(Samurai samurai)
        {
            Geometry g = new RectangleGeometry(samurai.Area);
            return new GeometryDrawing(this.samuraiBrush, null, g);
        }

        private Drawing GetDaimyo(Daimyo daimyo)
        {
            Geometry g = new RectangleGeometry(daimyo.Area);
            return new GeometryDrawing(this.daimyoBrush, null, g);
        }

        // Projectiles----------------------------------------------------------------------------------------------------------------------------
        private Drawing GetShuriken(Shuriken shuriken)
        {
            Geometry g = new RectangleGeometry(shuriken.Area);
            return new GeometryDrawing(this.shurikenBrush, null, g);
        }

        private Drawing GetArrow(Arrow arrow)
        {
            Geometry g = new RectangleGeometry(arrow.Area);
            return new GeometryDrawing(this.arrowBrush, null, g);
        }

        private Drawing GetSword(Sword sword)
        {
            Geometry g = new RectangleGeometry(sword.Area);
            return new GeometryDrawing(this.swordBrush, null, g);
        }

        private Drawing GetBackground()
        {
            Geometry g = new RectangleGeometry(new Rect(0, 0, this.width, this.height));

            string[] lvlNum = this.backgroundBrush.ImageSource.ToString().Split('.');

            int lvlFromBg = int.Parse(this.backgroundBrush.ImageSource.ToString().Split('.')[4].Last().ToString());

            if (lvlFromBg != int.Parse(this.gm.CurrentLevel.Last().ToString()))
            {
                lvlFromBg++;
                this.backgroundBrush = this.GetBrush("_backgroundLvl" + lvlFromBg.ToString() + ".bmp");
            }

            return new GeometryDrawing(this.backgroundBrush, null, g);
        }

        private ImageBrush GetBrush(string filename)
        {
            filename = @"..\..\" + filename;
            BitmapImage myBitmapImage = new BitmapImage(new Uri(filename, UriKind.Relative));

            ImageBrush ib = new ImageBrush(myBitmapImage);

            if (this.gm.CurrentLevel == "Level6")
            {
                filename = @"..\..\_backgroundLvl5.bmp";
            }

            if (Regex.IsMatch(filename, @"_backgroundLvl{1}[0-9].bmp") == true)
            {
                ib.TileMode = TileMode.None;
                ib.Viewport = new Rect(0, 0, myBitmapImage.Width, myBitmapImage.Height);
                ib.ViewportUnits = BrushMappingMode.Absolute;
            }

            return ib;
        }
    }
}
