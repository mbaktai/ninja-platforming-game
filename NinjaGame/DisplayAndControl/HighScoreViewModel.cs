﻿// <copyright file="HighScoreViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.WPF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using GalaSoft.MvvmLight;
    using NinjaGame.Logic;

    /// <summary>
    /// Represents a ViewModel for highscores.
    /// </summary>
    public class HighScoreViewModel : ViewModelBase
    {
        private XDocument doc = XDocument.Load(@"..\..\GameData.xml");

        private string[,] scores;
        private string[] level1;
        private string[] level2;
        private string[] level3;
        private string[] level4;
        private string[] level5;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreViewModel"/> class.
        /// </summary>
        public HighScoreViewModel()
        {
            this.scores = new string[5, 3];
            this.level1 = new string[3];
            this.Level2 = new string[3];
            this.Level3 = new string[3];
            this.Level4 = new string[3];
            this.Level5 = new string[3];

            this.GetResults();

            for (int i = 0; i < this.scores.GetLength(0); i++)
            {
                for (int j = 0; j < this.scores.GetLength(1); j++)
                {
                    switch (i)
                    {
                        case 0:
                            this.SplitArray(this.level1, j, this.scores[i, j]);
                            break;
                        case 1:
                            this.SplitArray(this.Level2, j, this.scores[i, j]);
                            break;
                        case 2:
                            this.SplitArray(this.Level3, j, this.scores[i, j]);
                            break;
                        case 3:
                            this.SplitArray(this.Level4, j, this.scores[i, j]);
                            break;
                        case 4:
                            this.SplitArray(this.Level5, j, this.scores[i, j]);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets Level1 highscores.
        /// </summary>
        public string[] Level1
        {
            get { return this.level1; }
            set { this.level1 = value; }
        }

        /// <summary>
        /// Gets or sets Level2 highscores.
        /// </summary>
        public string[] Level2
        {
            get { return this.level2; }
            set { this.level2 = value; }
        }

        /// <summary>
        /// Gets or sets Level3 highscores.
        /// </summary>
        public string[] Level3
        {
            get { return this.level3; }
            set { this.level3 = value; }
        }

        /// <summary>
        /// Gets or sets Level4 highscores.
        /// </summary>
        public string[] Level4
        {
            get { return this.level4; }
            set { this.level4 = value; }
        }

        /// <summary>
        /// Gets or sets Level5 highscores.
        /// </summary>
        public string[] Level5
        {
            get { return this.level5; }
            set { this.level5 = value; }
        }

        private void GetResults()
        {
            string[] places =
            {
                "First", "Second", "Third"
            };

            string[] levels =
            {
                "Level1", "Level2", "Level3", "Level4", "Level5"
            };

            int level = 0;

            while (level < levels.Length)
            {
                int place = 0;

                while (place < places.Length)
                {
                    var q = from x in this.doc.
                        Descendants("SavedValues").
                        Descendants("Time").Descendants(levels[level])
                            select x.Element(places[place]).Value;
                    TimeSpan ts = TimeSpan.Parse(q.ToList().First().ToString());
                    this.scores[level, place] = ts.ToString(@"mm\:ss\.fff");

                    place++;
                }

                level++;
            }
        }

        private void SplitArray(string[] arr, int idx, string val)
        {
            arr[idx] = val;
        }
    }
}