﻿// <copyright file="Tests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using NinjaGame.Data;
    using NinjaGame.Logic;
    using NinjaGame.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Contains tests.
    /// </summary>
    [TestFixture]
    internal class Tests
    {
        private Player player;
        private GameLogic logic;
        private GameModel model;

        /// <summary>
        /// Initial setup.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;
            path = path.Replace(@"\NinjaGame.Tests", @"\NinjaGame");
            Directory.SetCurrentDirectory(path);

            this.player = new Player();
            this.model = new GameModel();
            this.logic = new GameLogic(this.model, 500, 500);
            this.logic.LoadNextLevel();
        }

        /// <summary>
        /// Check if the player exists upon model creation.
        /// </summary>
        [Test]
        public void PlayerExists()
        {
            Assert.That(this.logic.NinjaGameModel.ActualPlayer != null);
        }

        /// <summary>
        /// Checks if the level can be increased.
        /// </summary>
        /// <param name="input">Input parameter.</param>
        /// <param name="expected">The expected value.</param>
        [TestCase("Level1", "Level2")]
        [TestCase("Level2", "Level3")]
        [TestCase("Level3", "Level4")]
        [TestCase("Level4", "Level5")]
        public void LevelCanBeIncreased(string input, string expected)
        {
            this.logic.NinjaGameModel.CurrentLevel = input;
            this.logic.IncreaseLevel();
            Assert.That(this.logic.NinjaGameModel.CurrentLevel == expected);
        }

        /// <summary>
        /// Checks if the falls upon calling FallPlayer().
        /// </summary>
        [Test]
        public void PlayerFalls()
        {
            this.logic.FallPlayer();
            Assert.That(this.logic.NinjaGameModel.ActualPlayer.DY > 0);
        }

        /// <summary>
        /// Checks if the platforms move upon calling MovePlatforms().
        /// </summary>
        [Test]
        public void PlatformMoves()
        {
            Assert.That(this.logic.NinjaGameModel.Platforms.Any(x => x.DX > 0));
        }

        /// <summary>
        /// Checks if the enemy count is higher than zero upon model logic creation.
        /// </summary>
        [Test]
        public void EnemiesExist()
        {
            Assert.That(this.logic.NinjaGameModel.CurrentEnemies.Count > 0);
        }

        /// <summary>
        /// Checks if projectile is added if the player shoots.
        /// </summary>
        [Test]
        public void ProjectilesCanBeAdded()
        {
            this.logic.PlayerShoot();
            Assert.That(this.logic.NinjaGameModel.Projectiles.Count > 0);
        }

        /// <summary>
        /// Checks if the player stops falling when StopPlayerFall() is called.
        /// </summary>
        [Test]
        public void FallingCanBeStopped()
        {
            this.logic.NinjaGameModel.ActualPlayer.DY = 1;
            this.logic.StopPlayerFall();
            Assert.That(this.logic.NinjaGameModel.ActualPlayer.DY == 0);
        }

        /// <summary>
        /// Checks if the game ends if the player has 0 health.
        /// </summary>
        [Test]
        public void GameEnds()
        {
            this.logic.IsPaused = false;
            this.logic.NinjaGameModel.ActualPlayer.Health = 0;
            this.logic.FallPlayer();
            Assert.That(this.logic.IsPaused == true);
        }

        /// <summary>
        /// Checks if the player jumps, then its DY property changes accordingly.
        /// </summary>
        [Test]
        public void PlayerCanJump()
        {
            this.logic.JumpTicks = 1;
            this.logic.Jump();
            Assert.That(this.logic.NinjaGameModel.ActualPlayer.DY == -1);
        }

        /// <summary>
        /// Checks if the player moves with the platform if it is standing on it.
        /// </summary>
        [Test]
        public void PlayerMovesWithPlatform()
        {
            this.logic.NinjaGameModel.ActualPlayer.Slide(1, 1, 1, 1);
            Assert.That(this.logic.NinjaGameModel.ActualPlayer.DX != 0);
        }

        /// <summary>
        /// Checks if the DropDown() method is called, its DY property changes accordingly.
        /// </summary>
        [Test]
        public void PlayerCanDropDown()
        {
            this.logic.DropDown();
            Assert.That(this.logic.NinjaGameModel.ActualPlayer.DY > 0);
        }

        /// <summary>
        /// Checks if the player moves right if MoveRight() is called.
        /// </summary>
        [Test]
        public void PlayerCanMoveRight()
        {
            this.logic.MoveRight();
            Assert.That(this.logic.NinjaGameModel.ActualPlayer.DX > 0);
        }

        /// <summary>
        /// Checks if the player moves left if MoveLeft() is called.
        /// </summary>
        [Test]
        public void PlayerCanMoveLeft()
        {
            this.logic.MoveLeft();
            Assert.That(this.logic.NinjaGameModel.ActualPlayer.DX < 0);
        }
    }
}
