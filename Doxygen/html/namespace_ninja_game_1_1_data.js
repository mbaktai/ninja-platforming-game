var namespace_ninja_game_1_1_data =
[
    [ "Archer", "class_ninja_game_1_1_data_1_1_archer.html", "class_ninja_game_1_1_data_1_1_archer" ],
    [ "Arrow", "class_ninja_game_1_1_data_1_1_arrow.html", "class_ninja_game_1_1_data_1_1_arrow" ],
    [ "Character", "class_ninja_game_1_1_data_1_1_character.html", "class_ninja_game_1_1_data_1_1_character" ],
    [ "Daimyo", "class_ninja_game_1_1_data_1_1_daimyo.html", "class_ninja_game_1_1_data_1_1_daimyo" ],
    [ "DataReader", "class_ninja_game_1_1_data_1_1_data_reader.html", "class_ninja_game_1_1_data_1_1_data_reader" ],
    [ "GameObject", "class_ninja_game_1_1_data_1_1_game_object.html", "class_ninja_game_1_1_data_1_1_game_object" ],
    [ "IDoesSomePickupStuff", "interface_ninja_game_1_1_data_1_1_i_does_some_pickup_stuff.html", "interface_ninja_game_1_1_data_1_1_i_does_some_pickup_stuff" ],
    [ "IShoots", "interface_ninja_game_1_1_data_1_1_i_shoots.html", "interface_ninja_game_1_1_data_1_1_i_shoots" ],
    [ "Pickup", "class_ninja_game_1_1_data_1_1_pickup.html", "class_ninja_game_1_1_data_1_1_pickup" ],
    [ "Platform", "class_ninja_game_1_1_data_1_1_platform.html", "class_ninja_game_1_1_data_1_1_platform" ],
    [ "Player", "class_ninja_game_1_1_data_1_1_player.html", "class_ninja_game_1_1_data_1_1_player" ],
    [ "Projectile", "class_ninja_game_1_1_data_1_1_projectile.html", "class_ninja_game_1_1_data_1_1_projectile" ],
    [ "Samurai", "class_ninja_game_1_1_data_1_1_samurai.html", "class_ninja_game_1_1_data_1_1_samurai" ],
    [ "Shogun", "class_ninja_game_1_1_data_1_1_shogun.html", "class_ninja_game_1_1_data_1_1_shogun" ],
    [ "Shuriken", "class_ninja_game_1_1_data_1_1_shuriken.html", "class_ninja_game_1_1_data_1_1_shuriken" ],
    [ "Sword", "class_ninja_game_1_1_data_1_1_sword.html", "class_ninja_game_1_1_data_1_1_sword" ]
];