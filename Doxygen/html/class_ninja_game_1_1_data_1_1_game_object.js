var class_ninja_game_1_1_data_1_1_game_object =
[
    [ "UpdateAreaX", "class_ninja_game_1_1_data_1_1_game_object.html#a32d54cc6a57d44ce0f3d22142ff93eb4", null ],
    [ "UpdateAreaY", "class_ninja_game_1_1_data_1_1_game_object.html#a755b210b314801d62fdb3f57ff75e2a9", null ],
    [ "UpdateX", "class_ninja_game_1_1_data_1_1_game_object.html#ad43466bf69055c9cd84ab94a2153da5a", null ],
    [ "UpdateY", "class_ninja_game_1_1_data_1_1_game_object.html#ae8711ec33bc9cc1c4e35a166ab781285", null ],
    [ "Area", "class_ninja_game_1_1_data_1_1_game_object.html#aa0bd5c8f82121e784c1973f95c5d5c9e", null ],
    [ "DX", "class_ninja_game_1_1_data_1_1_game_object.html#af5f33a5ace2bd7114d7002538aa257f0", null ],
    [ "DY", "class_ninja_game_1_1_data_1_1_game_object.html#a6612c9e43e642f1a27cd72458e2c55a9", null ],
    [ "Position", "class_ninja_game_1_1_data_1_1_game_object.html#acc5e2cdcf7dfe38f542e240135657f49", null ],
    [ "Texture", "class_ninja_game_1_1_data_1_1_game_object.html#a38f1fb5465d14535379e24da3d7c6222", null ]
];