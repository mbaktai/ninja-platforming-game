var namespace_ninja_game =
[
    [ "Data", "namespace_ninja_game_1_1_data.html", "namespace_ninja_game_1_1_data" ],
    [ "Logic", "namespace_ninja_game_1_1_logic.html", "namespace_ninja_game_1_1_logic" ],
    [ "Repository", "namespace_ninja_game_1_1_repository.html", "namespace_ninja_game_1_1_repository" ],
    [ "Tests", "namespace_ninja_game_1_1_tests.html", "namespace_ninja_game_1_1_tests" ],
    [ "WPF", "namespace_ninja_game_1_1_w_p_f.html", "namespace_ninja_game_1_1_w_p_f" ],
    [ "App", "class_ninja_game_1_1_app.html", "class_ninja_game_1_1_app" ],
    [ "MainWindow", "class_ninja_game_1_1_main_window.html", "class_ninja_game_1_1_main_window" ]
];