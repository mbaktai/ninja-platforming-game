var class_ninja_game_1_1_data_1_1_player =
[
    [ "Player", "class_ninja_game_1_1_data_1_1_player.html#a9a4e96b99274cfc96d24ff93c9723906", null ],
    [ "Fall", "class_ninja_game_1_1_data_1_1_player.html#ad9b0edd823d72ab0802f121fbf6aeb41", null ],
    [ "Jump", "class_ninja_game_1_1_data_1_1_player.html#a5505407467b2e827bacc98d3e77466df", null ],
    [ "MoveLeft", "class_ninja_game_1_1_data_1_1_player.html#a925be92368f44873855ecc80e1394370", null ],
    [ "MoveRight", "class_ninja_game_1_1_data_1_1_player.html#a81aa2a4dacb31f53228b5666884ffaa8", null ],
    [ "Slide", "class_ninja_game_1_1_data_1_1_player.html#a314e826333440ffac746e159f24523ed", null ],
    [ "CooldownMS", "class_ninja_game_1_1_data_1_1_player.html#a3eb5a84f79c9124a787d928981083805", null ],
    [ "DropRate", "class_ninja_game_1_1_data_1_1_player.html#a1151bab966e4483f1f306201964f42bf", null ],
    [ "JumpRate", "class_ninja_game_1_1_data_1_1_player.html#abaabee88091f79adaf8a2eb33158b2ff", null ],
    [ "ShootLastUse", "class_ninja_game_1_1_data_1_1_player.html#ad642adae99ac0d934d67ee6468cd69a3", null ],
    [ "Step", "class_ninja_game_1_1_data_1_1_player.html#a4ed3c2f72ccc5f9e0fc8def974a4c5c0", null ]
];