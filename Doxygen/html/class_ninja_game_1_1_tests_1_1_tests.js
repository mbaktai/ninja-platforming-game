var class_ninja_game_1_1_tests_1_1_tests =
[
    [ "EnemiesExist", "class_ninja_game_1_1_tests_1_1_tests.html#af9f60303366eac65d3a8fac53ea40851", null ],
    [ "FallingCanBeStopped", "class_ninja_game_1_1_tests_1_1_tests.html#a9a617699b970b6e8486813c3a90e17e6", null ],
    [ "GameEnds", "class_ninja_game_1_1_tests_1_1_tests.html#a7185cd1f81b1e5a4cf4bd2d1420d1e90", null ],
    [ "LevelCanBeIncreased", "class_ninja_game_1_1_tests_1_1_tests.html#a06f2aeb90295cb0b042335d62b8bd7b8", null ],
    [ "PlatformMoves", "class_ninja_game_1_1_tests_1_1_tests.html#aa7f1159bc43b2ffd1417c620d23f19a5", null ],
    [ "PlayerCanDropDown", "class_ninja_game_1_1_tests_1_1_tests.html#a7b0fa095c7a949d887685ad239bc6808", null ],
    [ "PlayerCanJump", "class_ninja_game_1_1_tests_1_1_tests.html#a19bacd853d0b3672ea0c1bf7c1cc0c1e", null ],
    [ "PlayerCanMoveLeft", "class_ninja_game_1_1_tests_1_1_tests.html#a43a893fbed45dbf7a681226e6950f546", null ],
    [ "PlayerCanMoveRight", "class_ninja_game_1_1_tests_1_1_tests.html#abf90b76744fcdda5a3713ae05f3f6a38", null ],
    [ "PlayerExists", "class_ninja_game_1_1_tests_1_1_tests.html#acab631f6d1df18c17afbb7573a53fa75", null ],
    [ "PlayerFalls", "class_ninja_game_1_1_tests_1_1_tests.html#a9b7e2246c5631970602a75c50f7e5605", null ],
    [ "PlayerMovesWithPlatform", "class_ninja_game_1_1_tests_1_1_tests.html#a99ae8d6201547a9ee50f52e3bee0ba57", null ],
    [ "ProjectilesCanBeAdded", "class_ninja_game_1_1_tests_1_1_tests.html#aa8bcbb262804708e1c7261b1ceca020c", null ],
    [ "Setup", "class_ninja_game_1_1_tests_1_1_tests.html#acabfa19819da30d9c1adc7013ad9bec3", null ]
];