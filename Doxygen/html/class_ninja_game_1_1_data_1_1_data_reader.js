var class_ninja_game_1_1_data_1_1_data_reader =
[
    [ "GetAGameObject", "class_ninja_game_1_1_data_1_1_data_reader.html#aeff5b40fe5d56e4584e1c8b4c0256af8", null ],
    [ "GetHighScore", "class_ninja_game_1_1_data_1_1_data_reader.html#a54cf614b3994076c68ca8ff902a48f17", null ],
    [ "ReadLevelHighscores", "class_ninja_game_1_1_data_1_1_data_reader.html#a9a4158ba5eb40f39350e206c6edd31b1", null ],
    [ "ReadNumberOfEnemies", "class_ninja_game_1_1_data_1_1_data_reader.html#a381ffa3b1821815f334840bb590c5488", null ],
    [ "ReadNumberOfSamurais", "class_ninja_game_1_1_data_1_1_data_reader.html#aa7804a00d8c1ec3d8df108ba07624d8b", null ],
    [ "ReadPlatformSpeed", "class_ninja_game_1_1_data_1_1_data_reader.html#a7dfcda29427f15aef0833e7916735224", null ],
    [ "SaveEnemyData", "class_ninja_game_1_1_data_1_1_data_reader.html#a11eaadc5dd83428062adc973975edf9b", null ],
    [ "SaveLevelData", "class_ninja_game_1_1_data_1_1_data_reader.html#a32b074add7ed44b7212b4d95ff63c028", null ],
    [ "SavePlatformData", "class_ninja_game_1_1_data_1_1_data_reader.html#a803cbe0829b9034a70d502fff294e88d", null ],
    [ "SavePlayerData", "class_ninja_game_1_1_data_1_1_data_reader.html#a29c39b6e6a8016d9f1542fbf893b26eb", null ],
    [ "SaveResults", "class_ninja_game_1_1_data_1_1_data_reader.html#a47ebb46a5e274d444f4b90707730a4d8", null ],
    [ "SaveTimeToXml", "class_ninja_game_1_1_data_1_1_data_reader.html#a725e0382bfad44321fe88c29a6a088da", null ],
    [ "Doc", "class_ninja_game_1_1_data_1_1_data_reader.html#a077883dea9a26bab9cf67ee091a0f187", null ]
];