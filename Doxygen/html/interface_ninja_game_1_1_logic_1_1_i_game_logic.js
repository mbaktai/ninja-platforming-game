var interface_ninja_game_1_1_logic_1_1_i_game_logic =
[
    [ "DropDown", "interface_ninja_game_1_1_logic_1_1_i_game_logic.html#a5d8710d6e049d3936b704fac988a5cf8", null ],
    [ "Jump", "interface_ninja_game_1_1_logic_1_1_i_game_logic.html#a9a6b468c85dd7e710e570d7a0cbd0edc", null ],
    [ "LoadGame", "interface_ninja_game_1_1_logic_1_1_i_game_logic.html#a540ef0b5874a0509ad8c16bbc6edbe68", null ],
    [ "MoveLeft", "interface_ninja_game_1_1_logic_1_1_i_game_logic.html#a219babeaa5f508b5eb7ed1c664762656", null ],
    [ "MoveRight", "interface_ninja_game_1_1_logic_1_1_i_game_logic.html#a0d1f297bda1aca9b846c20cf6f93dc86", null ],
    [ "PlayerShoot", "interface_ninja_game_1_1_logic_1_1_i_game_logic.html#a461a8cae58d0ca84406f70f7ce240ff0", null ],
    [ "Save", "interface_ninja_game_1_1_logic_1_1_i_game_logic.html#a2e549aa092cb6e1775728da9344491b2", null ],
    [ "SaveTime", "interface_ninja_game_1_1_logic_1_1_i_game_logic.html#ab5d12330fd13ec9ff5aa213fcce76e0c", null ],
    [ "Shoot", "interface_ninja_game_1_1_logic_1_1_i_game_logic.html#a83218468227c45118dedfed9af425a9c", null ]
];