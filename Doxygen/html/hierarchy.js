var hierarchy =
[
    [ "Application", null, [
      [ "NinjaGame.App", "class_ninja_game_1_1_app.html", null ],
      [ "NinjaGame.App", "class_ninja_game_1_1_app.html", null ],
      [ "NinjaGame.App", "class_ninja_game_1_1_app.html", null ],
      [ "NinjaGame.App", "class_ninja_game_1_1_app.html", null ],
      [ "NinjaGame.App", "class_ninja_game_1_1_app.html", null ]
    ] ],
    [ "ApplicationSettingsBase", null, [
      [ "NinjaGame.WPF.Properties.Settings", "class_ninja_game_1_1_w_p_f_1_1_properties_1_1_settings.html", null ]
    ] ],
    [ "NinjaGame.Data.DataReader", "class_ninja_game_1_1_data_1_1_data_reader.html", null ],
    [ "NinjaGame.WPF.Display", "class_ninja_game_1_1_w_p_f_1_1_display.html", null ],
    [ "FrameworkElement", null, [
      [ "NinjaGame.WPF.Control", "class_ninja_game_1_1_w_p_f_1_1_control.html", null ]
    ] ],
    [ "NinjaGame.Data.GameObject", "class_ninja_game_1_1_data_1_1_game_object.html", [
      [ "NinjaGame.Data.Character", "class_ninja_game_1_1_data_1_1_character.html", [
        [ "NinjaGame.Data.Archer", "class_ninja_game_1_1_data_1_1_archer.html", null ],
        [ "NinjaGame.Data.Daimyo", "class_ninja_game_1_1_data_1_1_daimyo.html", null ],
        [ "NinjaGame.Data.Player", "class_ninja_game_1_1_data_1_1_player.html", null ],
        [ "NinjaGame.Data.Samurai", "class_ninja_game_1_1_data_1_1_samurai.html", null ],
        [ "NinjaGame.Data.Shogun", "class_ninja_game_1_1_data_1_1_shogun.html", null ]
      ] ],
      [ "NinjaGame.Data.Pickup", "class_ninja_game_1_1_data_1_1_pickup.html", null ],
      [ "NinjaGame.Data.Platform", "class_ninja_game_1_1_data_1_1_platform.html", null ],
      [ "NinjaGame.Data.Projectile", "class_ninja_game_1_1_data_1_1_projectile.html", [
        [ "NinjaGame.Data.Arrow", "class_ninja_game_1_1_data_1_1_arrow.html", null ],
        [ "NinjaGame.Data.Shuriken", "class_ninja_game_1_1_data_1_1_shuriken.html", null ],
        [ "NinjaGame.Data.Sword", "class_ninja_game_1_1_data_1_1_sword.html", null ]
      ] ]
    ] ],
    [ "IComponentConnector", null, [
      [ "NinjaGame.MainWindow", "class_ninja_game_1_1_main_window.html", null ],
      [ "NinjaGame.MainWindow", "class_ninja_game_1_1_main_window.html", null ],
      [ "NinjaGame.MainWindow", "class_ninja_game_1_1_main_window.html", null ],
      [ "NinjaGame.MainWindow", "class_ninja_game_1_1_main_window.html", null ],
      [ "NinjaGame.WPF.HighScoreWindow", "class_ninja_game_1_1_w_p_f_1_1_high_score_window.html", null ],
      [ "NinjaGame.WPF.HighScoreWindow", "class_ninja_game_1_1_w_p_f_1_1_high_score_window.html", null ]
    ] ],
    [ "NinjaGame.Data.IDoesSomePickupStuff", "interface_ninja_game_1_1_data_1_1_i_does_some_pickup_stuff.html", [
      [ "NinjaGame.Data.Pickup", "class_ninja_game_1_1_data_1_1_pickup.html", null ]
    ] ],
    [ "NinjaGame.Logic.IGameLogic", "interface_ninja_game_1_1_logic_1_1_i_game_logic.html", [
      [ "NinjaGame.Logic.GameLogic", "class_ninja_game_1_1_logic_1_1_game_logic.html", null ]
    ] ],
    [ "NinjaGame.Repository.IGameModel", "interface_ninja_game_1_1_repository_1_1_i_game_model.html", [
      [ "NinjaGame.Repository.GameModel", "class_ninja_game_1_1_repository_1_1_game_model.html", null ]
    ] ],
    [ "INotifyPropertyChanged", null, [
      [ "NinjaGame.Data.Character", "class_ninja_game_1_1_data_1_1_character.html", null ],
      [ "NinjaGame.Repository.GameModel", "class_ninja_game_1_1_repository_1_1_game_model.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "NinjaGame.Repository.IRepository", "interface_ninja_game_1_1_repository_1_1_i_repository.html", [
      [ "NinjaGame.Repository.GameRepository", "class_ninja_game_1_1_repository_1_1_game_repository.html", null ]
    ] ],
    [ "NinjaGame.Data.IShoots", "interface_ninja_game_1_1_data_1_1_i_shoots.html", [
      [ "NinjaGame.Data.Archer", "class_ninja_game_1_1_data_1_1_archer.html", null ],
      [ "NinjaGame.Data.Player", "class_ninja_game_1_1_data_1_1_player.html", null ],
      [ "NinjaGame.Data.Samurai", "class_ninja_game_1_1_data_1_1_samurai.html", null ]
    ] ],
    [ "NinjaGame.WPF.Properties.Resources", "class_ninja_game_1_1_w_p_f_1_1_properties_1_1_resources.html", null ],
    [ "NinjaGame.Tests.Tests", "class_ninja_game_1_1_tests_1_1_tests.html", null ],
    [ "ViewModelBase", null, [
      [ "NinjaGame.WPF.HighScoreViewModel", "class_ninja_game_1_1_w_p_f_1_1_high_score_view_model.html", null ]
    ] ],
    [ "Window", null, [
      [ "NinjaGame.MainWindow", "class_ninja_game_1_1_main_window.html", null ],
      [ "NinjaGame.MainWindow", "class_ninja_game_1_1_main_window.html", null ],
      [ "NinjaGame.MainWindow", "class_ninja_game_1_1_main_window.html", null ],
      [ "NinjaGame.MainWindow", "class_ninja_game_1_1_main_window.html", null ],
      [ "NinjaGame.MainWindow", "class_ninja_game_1_1_main_window.html", null ],
      [ "NinjaGame.WPF.HighScoreWindow", "class_ninja_game_1_1_w_p_f_1_1_high_score_window.html", null ],
      [ "NinjaGame.WPF.HighScoreWindow", "class_ninja_game_1_1_w_p_f_1_1_high_score_window.html", null ],
      [ "NinjaGame.WPF.HighScoreWindow", "class_ninja_game_1_1_w_p_f_1_1_high_score_window.html", null ]
    ] ]
];