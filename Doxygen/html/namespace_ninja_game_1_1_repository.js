var namespace_ninja_game_1_1_repository =
[
    [ "GameModel", "class_ninja_game_1_1_repository_1_1_game_model.html", "class_ninja_game_1_1_repository_1_1_game_model" ],
    [ "GameRepository", "class_ninja_game_1_1_repository_1_1_game_repository.html", "class_ninja_game_1_1_repository_1_1_game_repository" ],
    [ "IGameModel", "interface_ninja_game_1_1_repository_1_1_i_game_model.html", "interface_ninja_game_1_1_repository_1_1_i_game_model" ],
    [ "IRepository", "interface_ninja_game_1_1_repository_1_1_i_repository.html", "interface_ninja_game_1_1_repository_1_1_i_repository" ]
];