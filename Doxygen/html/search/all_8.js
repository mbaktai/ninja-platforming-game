var searchData=
[
  ['jump',['Jump',['../class_ninja_game_1_1_data_1_1_daimyo.html#ad99ac230078f72ff1b4f4eeabd04cb40',1,'NinjaGame.Data.Daimyo.Jump()'],['../class_ninja_game_1_1_data_1_1_player.html#a5505407467b2e827bacc98d3e77466df',1,'NinjaGame.Data.Player.Jump()'],['../interface_ninja_game_1_1_logic_1_1_i_game_logic.html#a9a6b468c85dd7e710e570d7a0cbd0edc',1,'NinjaGame.Logic.IGameLogic.Jump()'],['../class_ninja_game_1_1_logic_1_1_game_logic.html#a0e6922c030e61d4785fc05611852b3d9',1,'NinjaGame.Logic.GameLogic.Jump()']]],
  ['jumprate',['JumpRate',['../class_ninja_game_1_1_data_1_1_daimyo.html#a9e4ee3a33733ff2ec9268c8d750b34ed',1,'NinjaGame.Data.Daimyo.JumpRate()'],['../class_ninja_game_1_1_data_1_1_player.html#abaabee88091f79adaf8a2eb33158b2ff',1,'NinjaGame.Data.Player.JumpRate()']]],
  ['jumpticks',['JumpTicks',['../class_ninja_game_1_1_logic_1_1_game_logic.html#a18f5ce61733918490b556e7ed256c09b',1,'NinjaGame::Logic::GameLogic']]]
];
