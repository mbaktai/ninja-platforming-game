var searchData=
[
  ['daimyojumpticks',['DaimyoJumpticks',['../class_ninja_game_1_1_repository_1_1_game_model.html#a9d57388a6e72e700f6c4a6dc49f1bf18',1,'NinjaGame::Repository::GameModel']]],
  ['damage',['Damage',['../class_ninja_game_1_1_data_1_1_character.html#a66127d1e2fbff91b7f21375c7661b96e',1,'NinjaGame.Data.Character.Damage()'],['../class_ninja_game_1_1_data_1_1_projectile.html#abda1e00f0c4e1ebe2230c0432974ba20',1,'NinjaGame.Data.Projectile.Damage()']]],
  ['doc',['Doc',['../class_ninja_game_1_1_data_1_1_data_reader.html#a077883dea9a26bab9cf67ee091a0f187',1,'NinjaGame::Data::DataReader']]],
  ['droprate',['DropRate',['../class_ninja_game_1_1_data_1_1_daimyo.html#a471972f295c2f3ce51a3043c83c08719',1,'NinjaGame.Data.Daimyo.DropRate()'],['../class_ninja_game_1_1_data_1_1_player.html#a1151bab966e4483f1f306201964f42bf',1,'NinjaGame.Data.Player.DropRate()']]],
  ['dx',['DX',['../class_ninja_game_1_1_data_1_1_game_object.html#af5f33a5ace2bd7114d7002538aa257f0',1,'NinjaGame::Data::GameObject']]],
  ['dy',['DY',['../class_ninja_game_1_1_data_1_1_game_object.html#a6612c9e43e642f1a27cd72458e2c55a9',1,'NinjaGame::Data::GameObject']]]
];
