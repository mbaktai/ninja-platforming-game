var indexSectionsWithContent =
{
  0: "acdefghijlmnoprstux",
  1: "acdghimprst",
  2: "nx",
  3: "acdefghijlmoprsu",
  4: "acdeghijlnprst",
  5: "pr",
  6: "cno"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "properties",
  5: "events",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Properties",
  5: "Events",
  6: "Pages"
};

