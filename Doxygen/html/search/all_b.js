var searchData=
[
  ['data',['Data',['../namespace_ninja_game_1_1_data.html',1,'NinjaGame']]],
  ['logic',['Logic',['../namespace_ninja_game_1_1_logic.html',1,'NinjaGame']]],
  ['nunit_203_2e12_20_2d_20may_2014_2c_202019',['NUnit 3.12 - May 14, 2019',['../md__e_1__o_e__prog4_s_z_t__f_f_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html',1,'']]],
  ['ninjagame',['NinjaGame',['../namespace_ninja_game.html',1,'']]],
  ['ninjagamemodel',['NinjaGameModel',['../class_ninja_game_1_1_logic_1_1_game_logic.html#ab292109dce035b8f138694f9030b5509',1,'NinjaGame::Logic::GameLogic']]],
  ['properties',['Properties',['../namespace_ninja_game_1_1_w_p_f_1_1_properties.html',1,'NinjaGame::WPF']]],
  ['repository',['Repository',['../namespace_ninja_game_1_1_repository.html',1,'NinjaGame']]],
  ['tests',['Tests',['../namespace_ninja_game_1_1_tests.html',1,'NinjaGame']]],
  ['wpf',['WPF',['../namespace_ninja_game_1_1_w_p_f.html',1,'NinjaGame']]]
];
