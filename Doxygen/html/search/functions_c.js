var searchData=
[
  ['platform',['Platform',['../class_ninja_game_1_1_data_1_1_platform.html#a87d68ff765e283822e8650a116764e05',1,'NinjaGame::Data::Platform']]],
  ['platformmoves',['PlatformMoves',['../class_ninja_game_1_1_tests_1_1_tests.html#aa7f1159bc43b2ffd1417c620d23f19a5',1,'NinjaGame::Tests::Tests']]],
  ['player',['Player',['../class_ninja_game_1_1_data_1_1_player.html#a9a4e96b99274cfc96d24ff93c9723906',1,'NinjaGame::Data::Player']]],
  ['playercandropdown',['PlayerCanDropDown',['../class_ninja_game_1_1_tests_1_1_tests.html#a7b0fa095c7a949d887685ad239bc6808',1,'NinjaGame::Tests::Tests']]],
  ['playercanjump',['PlayerCanJump',['../class_ninja_game_1_1_tests_1_1_tests.html#a19bacd853d0b3672ea0c1bf7c1cc0c1e',1,'NinjaGame::Tests::Tests']]],
  ['playercanmoveleft',['PlayerCanMoveLeft',['../class_ninja_game_1_1_tests_1_1_tests.html#a43a893fbed45dbf7a681226e6950f546',1,'NinjaGame::Tests::Tests']]],
  ['playercanmoveright',['PlayerCanMoveRight',['../class_ninja_game_1_1_tests_1_1_tests.html#abf90b76744fcdda5a3713ae05f3f6a38',1,'NinjaGame::Tests::Tests']]],
  ['playerexists',['PlayerExists',['../class_ninja_game_1_1_tests_1_1_tests.html#acab631f6d1df18c17afbb7573a53fa75',1,'NinjaGame::Tests::Tests']]],
  ['playerfalls',['PlayerFalls',['../class_ninja_game_1_1_tests_1_1_tests.html#a9b7e2246c5631970602a75c50f7e5605',1,'NinjaGame::Tests::Tests']]],
  ['playermoveswithplatform',['PlayerMovesWithPlatform',['../class_ninja_game_1_1_tests_1_1_tests.html#a99ae8d6201547a9ee50f52e3bee0ba57',1,'NinjaGame::Tests::Tests']]],
  ['playeronplatform',['PlayerOnPlatform',['../class_ninja_game_1_1_logic_1_1_game_logic.html#a870c29beb7599cb9a06276e7993b49b8',1,'NinjaGame::Logic::GameLogic']]],
  ['playershoot',['PlayerShoot',['../interface_ninja_game_1_1_logic_1_1_i_game_logic.html#a461a8cae58d0ca84406f70f7ce240ff0',1,'NinjaGame.Logic.IGameLogic.PlayerShoot()'],['../class_ninja_game_1_1_logic_1_1_game_logic.html#ae63357495994bd582f4a73d3284cefec',1,'NinjaGame.Logic.GameLogic.PlayerShoot()']]],
  ['projectilescanbeadded',['ProjectilesCanBeAdded',['../class_ninja_game_1_1_tests_1_1_tests.html#aa8bcbb262804708e1c7261b1ceca020c',1,'NinjaGame::Tests::Tests']]]
];
