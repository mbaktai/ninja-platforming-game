var searchData=
[
  ['data',['Data',['../namespace_ninja_game_1_1_data.html',1,'NinjaGame']]],
  ['logic',['Logic',['../namespace_ninja_game_1_1_logic.html',1,'NinjaGame']]],
  ['ninjagame',['NinjaGame',['../namespace_ninja_game.html',1,'']]],
  ['properties',['Properties',['../namespace_ninja_game_1_1_w_p_f_1_1_properties.html',1,'NinjaGame::WPF']]],
  ['repository',['Repository',['../namespace_ninja_game_1_1_repository.html',1,'NinjaGame']]],
  ['tests',['Tests',['../namespace_ninja_game_1_1_tests.html',1,'NinjaGame']]],
  ['wpf',['WPF',['../namespace_ninja_game_1_1_w_p_f.html',1,'NinjaGame']]]
];
