var searchData=
[
  ['idoessomepickupstuff',['IDoesSomePickupStuff',['../interface_ninja_game_1_1_data_1_1_i_does_some_pickup_stuff.html',1,'NinjaGame::Data']]],
  ['igamelogic',['IGameLogic',['../interface_ninja_game_1_1_logic_1_1_i_game_logic.html',1,'NinjaGame::Logic']]],
  ['igamemodel',['IGameModel',['../interface_ninja_game_1_1_repository_1_1_i_game_model.html',1,'NinjaGame::Repository']]],
  ['increaselevel',['IncreaseLevel',['../class_ninja_game_1_1_logic_1_1_game_logic.html#a0f1b2dc7f4b0c9623bf560cbba35621e',1,'NinjaGame::Logic::GameLogic']]],
  ['initializecomponent',['InitializeComponent',['../class_ninja_game_1_1_app.html#a4b757578c0057466c025e3ee0ba067ff',1,'NinjaGame.App.InitializeComponent()'],['../class_ninja_game_1_1_app.html#a4b757578c0057466c025e3ee0ba067ff',1,'NinjaGame.App.InitializeComponent()'],['../class_ninja_game_1_1_w_p_f_1_1_high_score_window.html#a5347201b78f899a64d265baa18cd6e2f',1,'NinjaGame.WPF.HighScoreWindow.InitializeComponent()'],['../class_ninja_game_1_1_w_p_f_1_1_high_score_window.html#a5347201b78f899a64d265baa18cd6e2f',1,'NinjaGame.WPF.HighScoreWindow.InitializeComponent()'],['../class_ninja_game_1_1_main_window.html#aa9de28178efe8437a21cff7b373c5111',1,'NinjaGame.MainWindow.InitializeComponent()'],['../class_ninja_game_1_1_main_window.html#aa9de28178efe8437a21cff7b373c5111',1,'NinjaGame.MainWindow.InitializeComponent()'],['../class_ninja_game_1_1_app.html#a4b757578c0057466c025e3ee0ba067ff',1,'NinjaGame.App.InitializeComponent()'],['../class_ninja_game_1_1_app.html#a4b757578c0057466c025e3ee0ba067ff',1,'NinjaGame.App.InitializeComponent()'],['../class_ninja_game_1_1_main_window.html#aa9de28178efe8437a21cff7b373c5111',1,'NinjaGame.MainWindow.InitializeComponent()'],['../class_ninja_game_1_1_main_window.html#aa9de28178efe8437a21cff7b373c5111',1,'NinjaGame.MainWindow.InitializeComponent()']]],
  ['irepository',['IRepository',['../interface_ninja_game_1_1_repository_1_1_i_repository.html',1,'NinjaGame::Repository']]],
  ['isdead',['IsDead',['../class_ninja_game_1_1_data_1_1_character.html#ac18fb8e0d6416d91aecf10b81e1701f3',1,'NinjaGame::Data::Character']]],
  ['ishoots',['IShoots',['../interface_ninja_game_1_1_data_1_1_i_shoots.html',1,'NinjaGame::Data']]],
  ['ishorizontal',['IsHorizontal',['../class_ninja_game_1_1_data_1_1_platform.html#ababfcf75489cedd9bc8cb64042a692dc',1,'NinjaGame::Data::Platform']]],
  ['ispaused',['IsPaused',['../class_ninja_game_1_1_logic_1_1_game_logic.html#ac7729ed4fa4022b008d091c64dfd1b24',1,'NinjaGame::Logic::GameLogic']]],
  ['isplayerplatform',['IsPlayerPlatform',['../class_ninja_game_1_1_data_1_1_platform.html#a112851c5e7c3e828bc0015bc3812682a',1,'NinjaGame::Data::Platform']]]
];
