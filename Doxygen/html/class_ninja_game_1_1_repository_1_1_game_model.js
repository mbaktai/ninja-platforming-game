var class_ninja_game_1_1_repository_1_1_game_model =
[
    [ "GameModel", "class_ninja_game_1_1_repository_1_1_game_model.html#a28e65130e755853b398baaef6ba067ea", null ],
    [ "OnPropertyChanged", "class_ninja_game_1_1_repository_1_1_game_model.html#af1dad51994eaabdeab2a7ae6d7c1f294", null ],
    [ "ActualPlayer", "class_ninja_game_1_1_repository_1_1_game_model.html#ab1c7ba77e4facc6827ad3e7e6e5b683a", null ],
    [ "CurrentEnemies", "class_ninja_game_1_1_repository_1_1_game_model.html#ac6cdba880496be2a5914f970264df171", null ],
    [ "CurrentLevel", "class_ninja_game_1_1_repository_1_1_game_model.html#a420de26a712c7dae8af70ba056eec2cc", null ],
    [ "DaimyoJumpticks", "class_ninja_game_1_1_repository_1_1_game_model.html#a9d57388a6e72e700f6c4a6dc49f1bf18", null ],
    [ "EnemiesLeft", "class_ninja_game_1_1_repository_1_1_game_model.html#af252069d6aae63e1cbd18904b8ddf472", null ],
    [ "GameFinsihed", "class_ninja_game_1_1_repository_1_1_game_model.html#a29841b9cfa924cb36ff6173d5f15a0a3", null ],
    [ "GameRepository", "class_ninja_game_1_1_repository_1_1_game_model.html#ae39a99218f152a29ceec45d2519d0d02", null ],
    [ "LevelStopwatch", "class_ninja_game_1_1_repository_1_1_game_model.html#a0ffbd2e12ba3f4cd3371cd0b52e2fb51", null ],
    [ "Platforms", "class_ninja_game_1_1_repository_1_1_game_model.html#a213c4175327ec3e78a3c0436b6f38bcb", null ],
    [ "Projectiles", "class_ninja_game_1_1_repository_1_1_game_model.html#acdcba4fd5bbf0c3f24ef0bd6fbfc93aa", null ],
    [ "RemainingEnemies", "class_ninja_game_1_1_repository_1_1_game_model.html#aabe70887ccfec9b138f97527286125e8", null ],
    [ "Results", "class_ninja_game_1_1_repository_1_1_game_model.html#af5332110dbdd85c54ed363ecaab6481e", null ],
    [ "PropertyChanged", "class_ninja_game_1_1_repository_1_1_game_model.html#a2e1a3627d587008f1e9bf0914f429a07", null ]
];