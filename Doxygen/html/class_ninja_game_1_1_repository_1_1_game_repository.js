var class_ninja_game_1_1_repository_1_1_game_repository =
[
    [ "GameRepository", "class_ninja_game_1_1_repository_1_1_game_repository.html#a374f5c13c64919153daed1c9985690f7", null ],
    [ "GetAGameObject", "class_ninja_game_1_1_repository_1_1_game_repository.html#a41aacee6ac49b28332eae5efaa3a7afd", null ],
    [ "ReadNumberOfEnemiesForLevel", "class_ninja_game_1_1_repository_1_1_game_repository.html#a857701de79d17934d21dbd19bebdb27c", null ],
    [ "ReadNumberOfSamuraiForLevel", "class_ninja_game_1_1_repository_1_1_game_repository.html#a00f527cf6b1266437c5a964c1037b60d", null ],
    [ "ReadPlatformSpeed", "class_ninja_game_1_1_repository_1_1_game_repository.html#a515bcc9e2c6abf7a8970c46ae9c731ec", null ]
];