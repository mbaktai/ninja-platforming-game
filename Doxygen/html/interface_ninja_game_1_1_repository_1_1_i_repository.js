var interface_ninja_game_1_1_repository_1_1_i_repository =
[
    [ "GetAGameObject", "interface_ninja_game_1_1_repository_1_1_i_repository.html#a1e91efc0e56b6f8e1e39fb35be895fad", null ],
    [ "ReadNumberOfEnemiesForLevel", "interface_ninja_game_1_1_repository_1_1_i_repository.html#ac35ee9f261d4335a2dfb179dcfb54c25", null ],
    [ "ReadNumberOfSamuraiForLevel", "interface_ninja_game_1_1_repository_1_1_i_repository.html#afd3ff0f4a3705283f3c94f30c1aa425b", null ],
    [ "ReadPlatformSpeed", "interface_ninja_game_1_1_repository_1_1_i_repository.html#a05ad55b49342ecf11709eba3308b1a6d", null ]
];