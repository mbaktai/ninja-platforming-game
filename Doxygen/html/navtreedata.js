/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "OENIK_PROG4_2019_1_RNYC8Y_MWZPCE", "index.html", [
    [ "Castle Core Changelog", "md__e_1__o_e__prog4_s_z_t__f_f_packages__castle_8_core_84_84_80__c_h_a_n_g_e_l_o_g.html", null ],
    [ "NUnit 3.12 - May 14, 2019", "md__e_1__o_e__prog4_s_z_t__f_f_packages__n_unit_83_812_80__c_h_a_n_g_e_s.html", null ],
    [ "OENIK_PROG4_19_1_RNYC8Y_MWZPCE", "md__e_1__o_e__prog4_s_z_t__f_f__r_e_a_d_m_e.html", null ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Properties", "functions_prop.html", null ],
        [ "Events", "functions_evnt.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_app_8xaml_8cs_source.html",
"class_ninja_game_1_1_w_p_f_1_1_display.html#ab1c98e00ffbe1dd94bc007a331f98f79"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';