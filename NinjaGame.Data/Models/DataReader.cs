﻿// <copyright file="DataReader.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using System.Xml.Linq;

    /// <summary>
    /// Represents methods for reading data for different gameobjects.
    /// </summary>
    public static class DataReader
    {
        private static XDocument doc = XDocument.Load(@"..\..\GameData.xml");
        private static XDocument xdoc = XDocument.Load(@"..\..\Saves\SaveExample.xml");

        /// <summary>
        /// Gets or sets the XML document to use.
        /// </summary>
        public static XDocument Doc
        {
            get { return doc; }
            set { doc = value; }
        }

        /// <summary>
        /// Read a game object, then returns it.
        /// </summary>
        /// <param name="gameObjectType">The type of the game object.</param>
        /// <returns>A game object of the specified type.</returns>
        public static GameObject GetAGameObject(string gameObjectType)
        {
            List<string> characters = new List<string>()
                {
                "Player",
                "Archer",
                "Shogun",
                "Samurai",
                "Daimyo"
            };

            if (gameObjectType != "Platform")
            {
                if (characters.Contains(gameObjectType))
                {
                    string[] hpDmgPic = CopyDataForCharacters(gameObjectType);
                    if (gameObjectType == "Player")
                    {
                        return new Player()
                        {
                            Health = int.Parse(hpDmgPic[0]),
                            Damage = int.Parse(hpDmgPic[1]),
                            Texture = hpDmgPic[2]
                        };
                    }
                    else if (gameObjectType == "Archer")
                    {
                        return new Archer()
                        {
                            Health = int.Parse(hpDmgPic[0]),
                            Damage = int.Parse(hpDmgPic[1]),
                            Texture = hpDmgPic[2]
                        };
                    }
                    else if (gameObjectType == "Samurai")
                    {
                        return new Samurai()
                        {
                            Health = int.Parse(hpDmgPic[0]),
                            Damage = int.Parse(hpDmgPic[1]),
                            Texture = hpDmgPic[2]
                        };
                    }
                    else if (gameObjectType == "Daimyo")
                    {
                        return new Daimyo()
                        {
                            Health = int.Parse(hpDmgPic[0]),
                            Damage = int.Parse(hpDmgPic[1]),
                            Texture = hpDmgPic[2]
                        };
                    }
                    else
                    {
                        return new Shogun()
                        {
                            Health = int.Parse(hpDmgPic[0]),
                            Damage = int.Parse(hpDmgPic[1]),
                            Texture = hpDmgPic[2]
                        };
                    }
                }
                else if (gameObjectType == "Shuriken")
                {
                    BitmapImage shrkimage = new BitmapImage(new Uri(@"..\..\_shuriken.bmp", UriKind.Relative));
                    Shuriken shrk = new Shuriken();
                    shrk.Area = new Rect(0, 0, shrkimage.Width, shrkimage.Height);

                    return shrk;
                }
                else if (gameObjectType == "Sword")
                {
                    BitmapImage kataimage = new BitmapImage(new Uri(@"..\..\_katana.bmp", UriKind.Relative));
                    Sword swrd = new Sword();
                    swrd.Area = new Rect(0, 0, kataimage.Width, kataimage.Height);

                    return swrd;
                }
                else
                {
                    BitmapImage arwimage = new BitmapImage(new Uri(@"..\..\_arrow.bmp", UriKind.Relative));
                    Arrow arw = new Arrow();
                    arw.Area = new Rect(0, 0, arwimage.Width, arwimage.Height);

                    return arw;
                }
            }
            else
            {
                // Helyes
                var platf = doc.Descendants(gameObjectType).
                    Select(x => new Platform()
                    {
                        Texture = x.Element("PicName").Value
                    });

                return platf.First();
            }
        }

        /// <summary>
        /// Saves current results.
        /// </summary>
        /// <param name="results">Results on each level.</param>
        public static void SaveResults(string[] results)
        {
            string[] levelNames = { "Level1", "Level2", "Level3", "Level4", "Level5" };

            for (int i = 0; i < results.Length; i++)
            {
                if (results[i] != null)
                {
                    SetResults(levelNames[i], results[i]);
                }
            }

            xdoc.Save("SaveExample.xml");
        }

        /// <summary>
        /// Saves time for the level.
        /// </summary>
        /// <param name="level">The level's name.</param>
        /// <param name="times">The times to write.</param>
        public static void SaveTimeToXml(string level, List<TimeSpan> times)
        {
            string[] places =
            {
                "First", "Second", "Third"
            };

            int i = 0;

            foreach (var actTime in times)
            {
                if (i < times.Count)
                {
                    doc.Descendants("SavedValues").Descendants("Time").Descendants(level).Descendants(places[i]).First().SetValue(times.ElementAt(i).ToString());
                    doc.Save(@"..\..\GameData.xml");
                }

                i++;
            }
        }

        /// <summary>
        /// ReadsHighScore
        /// </summary>
        /// <param name="level">name of level</param>
        /// <returns>high score for that level</returns>
        public static string GetHighScore(string level)
        {
            var q = (from x in doc.Descendants("SavedValues")
                    .Descendants("Time")
                    .Descendants()
                     select x.Element(level).Value).Single();
            return q;
        }

        /// <summary>
        /// Reads saved times from XML.
        /// </summary>
        /// <returns>A list with the level's highscore times</returns>
        /// <param name="level">The level to read.</param>
        public static List<TimeSpan> ReadLevelHighscores(string level)
        {
            List<TimeSpan> results = new List<TimeSpan>();

            string[] places =
            {
                "First", "Second", "Third"
            };

            int i = 0;

            while (i < 3)
            {
                var q = from x in doc.
                    Descendants("SavedValues").
                    Descendants("Time").Descendants(level)
                        select x.Element(places[i++]).Value;
                TimeSpan ts = TimeSpan.Parse(q.ToList().First().ToString());

                results.Add(ts);
            }

            return results;
        }

        /// <summary>
        /// Read No of enemies from xml
        /// </summary>
        /// <param name="level">name of level</param>
        /// <returns>no of enemies</returns>
        public static int ReadNumberOfEnemies(string level)
        {
            var q = from x in doc.
                    Descendants("BaseValues").
                    Descendants("Levels").
                    Descendants(level)
                    select x.Element("NumberOfEnemies").Value;

            return int.Parse(q.First().ToString());
        }

        /// <summary>
        /// No of samurais from xml
        /// </summary>
        /// <param name="level">name of lvl</param>
        /// <returns>no of samurais</returns>
        public static int ReadNumberOfSamurais(string level)
        {
            var q = from x in doc.
                    Descendants("BaseValues").
                    Descendants("Levels").
                    Descendants(level)
                    select x.Element("NoOfSamurai").Value;

            return int.Parse(q.First().ToString());
        }

        /// <summary>
        /// Saves data regarding platforms.
        /// </summary>
        /// <param name="platformName">Platforms name.</param>
        /// <param name="x">The top-left X value.</param>
        /// <param name="y">The top-left Y value.</param>
        /// <param name="dx">Moving direction.</param>
        public static void SavePlatformData(string platformName, double x, double y, int dx)
        {
            SetPlatformData(platformName, "X", x.ToString());
            SetPlatformData(platformName, "Y", y.ToString());
            SetPlatformData(platformName, "DX", dx.ToString());

            xdoc.Save("SaveExample.xml");
        }

        /// <summary>
        /// Platform speed from xml.
        /// </summary>
        /// <param name="level">nameofLevel</param>
        /// <returns>PlatformSpeed</returns>
        public static int ReadPlatformSpeed(string level)
        {
            var q = from x in doc.
                    Descendants("BaseValues").
                    Descendants("Levels").
                    Descendants(level)
                     select x.Element("PlatformSpeed").Value;

            return int.Parse(q.First().ToString());
        }

        /// <summary>
        /// Saves data regarding players.
        /// </summary>
        /// <param name="hp">Health.</param>
        /// <param name="x">Top-left X value.</param>
        /// <param name="y">Top-left Y value.</param>
        /// <param name="dx">X direction movement.</param>
        /// <param name="dy">Y direction movement.</param>
        public static void SavePlayerData(int hp, double x, double y, int dx, int dy)
        {
            SetDataForPlayer("Health", hp.ToString());
            SetDataForPlayer("X", x.ToString());
            SetDataForPlayer("Y", y.ToString());
            SetDataForPlayer("DX", dx.ToString());
            SetDataForPlayer("DY", dy.ToString());

            xdoc.Save("SaveExample.xml");
        }

        /// <summary>
        /// Saves data regarding enemy character.
        /// </summary>
        /// <param name="name">Name of the character.</param>
        /// <param name="type">Type of the character.</param>
        /// <param name="hp">Health.</param>
        /// <param name="x">Top-left X value.</param>
        /// <param name="y">Top left Y value.</param>
        public static void SaveEnemyData(string name, string type, int hp, double x, double y)
        {
            SetDataForEnemy(name, "Type", type);
            SetDataForEnemy(name, "Health", hp.ToString());
            SetDataForEnemy(name, "X", x.ToString());
            SetDataForEnemy(name, "Y", y.ToString());

            xdoc.Save("SaveExample.xml");
        }

        /// <summary>
        /// Save data regarding levels.
        /// </summary>
        /// <param name="level">Level name.</param>
        /// <param name="time">Current time.</param>
        /// <param name="remEnemy">Remaining enemies.</param>
        /// <param name="currEnemy">Current enemies.</param>
        /// <param name="remSamurai">Remaining samurais.</param>
        /// <param name="daimJumpTicks">Jump ticks for daimyo.</param>
        /// <param name="jumpTicks">Jump ticks for player.</param>
        public static void SaveLevelData(string level, string time, int remEnemy, int currEnemy, int remSamurai, int daimJumpTicks, int jumpTicks)
        {
            SetLevelData("Level", level);
            SetLevelData("CurrentTime", time);
            SetLevelData("RemainingEnemy", remEnemy.ToString());
            SetLevelData("CurrentEnemyNo", currEnemy.ToString());
            SetLevelData("RemainingSamurai", remSamurai.ToString());
            SetLevelData("DaimyoJumpTicks", daimJumpTicks.ToString());
            SetLevelData("JumpTicks", jumpTicks.ToString());

            xdoc.Save("SaveExample.xml");
        }

        private static void SetDataForEnemy(string name, string attribute, string value)
        {
            xdoc.Descendants("Characters").Descendants(name).Descendants().Where(x => x.Name == attribute).First().SetValue(value);
        }

        private static void SetLevelData(string attribute, string value)
        {
            xdoc.Descendants("SavedGame").Descendants().Where(x => x.Name == attribute).First().SetValue(value);
        }

        private static void SetDataForPlayer(string attribute, string value)
        {
            xdoc.Descendants("Characters").Descendants("Player").Descendants().Where(x => x.Name == attribute).First().SetValue(value);
        }

        private static void SetResults(string attribute, string value)
        {
            xdoc.Descendants("Results").Descendants().Where(x => x.Name == attribute).First().SetValue(value);
        }

        private static void SetPlatformData(string name, string attribute, string value)
        {
            xdoc.Descendants("Platforms").Descendants(name).Descendants().Where(x => x.Name == attribute).First().SetValue(value);
        }

        private static string[] CopyDataForCharacters(string gameObjectType)
        {
            var hp = from x in doc.
                    Descendants("BaseValues").
                    Descendants("GameObjects").
                    Descendants("Player")
                    select x.Element("Health").Value;

            var dmg = from x in doc.
                    Descendants("BaseValues").
                    Descendants("GameObjects").
                    Descendants("Player")
                    select x.Element("Damage").Value;

            var pic = from x in doc.
                    Descendants("BaseValues").
                    Descendants("GameObjects").
                    Descendants("Player")
                      select x.Element("PicName").Value;

            string[] data = new string[3];
            data[0] = hp.First().ToString();
            data[1] = dmg.First().ToString();
            data[2] = pic.First().ToString();

            return data;
        }
    }
}
