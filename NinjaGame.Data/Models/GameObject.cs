﻿// <copyright file="GameObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Root-class for every object the game uses on the screen (e.g.: projectiles, characters, pick-ups).
    /// </summary>
    public abstract class GameObject
    {
        private Rect area;
        private Point position;
        private string texture;
        private double dX;
        private double dY;

        /// <summary>
        /// Gets or sets DY.
        /// </summary>
        public double DY
        {
            get { return this.dY; }
            set { this.dY = value; }
        }

        /// <summary>
        /// Gets or sets DX.
        /// </summary>
        public double DX
        {
            get { return this.dX; }
            set { this.dX = value; }
        }

        /// <summary>
        /// Gets or sets the position on the gameobject.
        /// </summary>
        public Point Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        /// <summary>
        /// Gets or sets the area of the gameobject.
        /// </summary>
        public Rect Area
        {
            get { return this.area; }
            set { this.area = value; }
        }

        /// <summary>
        /// Gets or sets the texture (displayed image) of the gameobject.
        /// </summary>
        public string Texture
        {
            get { return this.texture; }
            set { this.texture = value; }
        }

        /// <summary>
        /// Updates the <see cref="Rect"/> area representation's X value.
        /// </summary>
        /// <param name="newX">Amount to move.</param>
        /// <param name="leftBound">The left bound of the gamespace.</param>
        /// <param name="rightBound">The Right bound of the gamespace.</param>
        public void UpdateAreaX(double newX, double leftBound, double rightBound)
        {
            if (this.GetType() == typeof(Player) && this.area.Left + newX >= leftBound && this.area.Right + newX <= rightBound)
            {
                this.area.X += newX;
                return;
            }
            else if (this.GetType() == typeof(Player) && (this.area.Left + newX <= leftBound || this.area.Right + newX >= rightBound))
            {
                return;
            }

            this.area.X += newX;
        }

        /// <summary>
        /// Updates the <see cref="Rect"/> area representation's Y value.
        /// </summary>
        /// <param name="newY">The new value to update with.</param>
        public void UpdateAreaY(double newY)
        {
            this.area.Y += newY;
        }

        /// <summary>
        /// Updates the <see cref="Rect"/> area representation's bottom-left Y value with an exact value.
        /// </summary>
        /// <param name="newY">The new value to update to.</param>
        public void UpdateY(double newY)
        {
            this.area.Location = new Point(this.area.TopLeft.X, newY - this.area.Height);
        }

        /// <summary>
        /// Updates the <see cref="Rect"/> area representation's top-left X value with an exact value.
        /// </summary>
        /// <param name="newX">The new value to update to.</param>
        public void UpdateX(double newX)
        {
            this.area.Location = new Point(newX, this.Area.TopLeft.Y);
        }
    }
}