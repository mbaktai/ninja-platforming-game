﻿// <copyright file="Projectile.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represent a displayed projectile.
    /// </summary>
    public abstract class Projectile : GameObject
    {
        private int damage;
        private int step;

        /// <summary>
        /// Gets or sets the projectiles damage.
        /// </summary>
        public int Damage
        {
            get { return this.damage; }
            set { this.damage = value; }
        }

        /// <summary>
        /// Gets or sets the step rate for the projectile.
        /// </summary>
        public int Step
        {
            get { return this.step; }
            set { this.step = value; }
        }

        /// <summary>
        /// Moves the projectile.
        /// </summary>
        /// <param name="leftBound">The left boundary of the gamespace.</param>
        /// <param name="rightBound">The Right boundary of the gamespace.</param>
        public void Move(double leftBound, double rightBound)
        {
            this.UpdateAreaX(this.DX * this.Step, leftBound, rightBound);
        }
    }
}
