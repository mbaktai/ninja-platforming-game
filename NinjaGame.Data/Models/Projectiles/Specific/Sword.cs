﻿// <copyright file="Sword.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents a kunai projectile.
    /// </summary>
    public class Sword : Projectile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Sword"/> class.
        /// </summary>
        public Sword()
        {
            this.DX = -1;
            this.DY = 0;
            this.Step = 10;
        }
    }
}
