﻿// <copyright file="Shuriken.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents an shuriken projectile.
    /// </summary>
    public class Shuriken : Projectile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Shuriken"/> class.
        /// </summary>
        public Shuriken()
        {
            this.DX = 1;
            this.DY = 0;
            this.Step = 26;
        }
    }
}
