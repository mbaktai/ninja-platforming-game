﻿// <copyright file="Arrow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents an arrow projectile.
    /// </summary>
    public class Arrow : Projectile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Arrow"/> class.
        /// </summary>
        public Arrow()
        {
            this.DX = -1;
            this.DY = 0;
            this.Step = 12;
        }
    }
}
