﻿// <copyright file="Platform.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Represents a platform.
    /// </summary>
    public class Platform : GameObject
    {
        private bool isHorizontal;
        private bool isPlayerPlatform;

        private int step;

        /// <summary>
        /// Initializes a new instance of the <see cref="Platform"/> class.
        /// </summary>
        public Platform()
        {
            this.DX = 1;
            this.DY = 0;
            this.Step = 2;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the platform is vertical or not.
        /// </summary>
        public bool IsHorizontal
        {
            get { return this.isHorizontal; }
            set { this.isHorizontal = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the platform is on the player's side or not.
        /// </summary>
        public bool IsPlayerPlatform
        {
            get { return this.isPlayerPlatform; }
            set { this.isPlayerPlatform = value; }
        }

        /// <summary>
        /// Gets or sets the step rate for the platform.
        /// </summary>
        public int Step
        {
            get { return this.step; }
            set { this.step = value; }
        }

        /// <summary>
        /// Moves the platform if it is on the player's side.
        /// </summary>
        /// <param name="leftBound">The left boundary of the gamespace.</param>
        /// <param name="rightBoud">The Right boundary of the gamespace.</param>
        public void Move(double leftBound, double rightBoud)
        {
            this.UpdateAreaX(this.DX * this.Step, leftBound, rightBoud);
        }
    }
}
