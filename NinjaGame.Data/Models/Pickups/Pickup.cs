﻿// <copyright file="Pickup.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represent a displayed pick-up.
    /// </summary>
    public abstract class Pickup : GameObject, IDoesSomePickupStuff
    {
        /// <inheritdoc/>
        public virtual void DoPickup()
        {
            throw new NotImplementedException();
        }
    }
}
