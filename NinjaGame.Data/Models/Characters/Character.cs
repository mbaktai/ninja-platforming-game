﻿// <copyright file="Character.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represent a displayed character, be it the player or an enemy.
    /// </summary>
    public abstract class Character : GameObject, INotifyPropertyChanged
    {
        private int health;
        private bool isDead;
        private int damage;

        /// <summary>
        /// Event for when a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the health of the character.
        /// </summary>
        public int Health
        {
            get
            {
                return this.health;
            }

            set
            {
                this.health = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the character is alive or not.
        /// </summary>
        public bool IsDead
        {
            get { return this.isDead; }
            set { this.isDead = value; }
        }

        /// <summary>
        /// Gets or sets the value of the characters damage output.
        /// </summary>
        public int Damage
        {
            get { return this.damage; }
            set { this.damage = value; }
        }

        /// <summary>
        /// Notifies when the property has changed.
        /// </summary>
        /// <param name="s">The property that has changed.</param>
        protected void OnPropertyChanged([CallerMemberName] string s = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(s));
        }
    }
}
