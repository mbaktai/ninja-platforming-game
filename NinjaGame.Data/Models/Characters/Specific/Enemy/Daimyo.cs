﻿// <copyright file="Daimyo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents an enemy daimyo character.
    /// </summary>
    public class Daimyo : Character
    {
        private int dropRate;
        private int jumpRate;
        private DateTime lastMove;

        /// <summary>
        /// Initializes a new instance of the <see cref="Daimyo"/> class.
        /// </summary>
        public Daimyo()
        {
            this.DX = 0;
            this.DY = 0;
        }

        /// <summary>
        /// Gets or sets the date of the last move of the daimyo character.
        /// </summary>
        public DateTime LastMove
        {
            get { return this.lastMove; }
            set { this.lastMove = value; }
        }

        /// <summary>
        /// Gets or sets the jumprate of the daimyo character.
        /// </summary>
        public int JumpRate
        {
            get { return this.jumpRate; }
            set { this.jumpRate = value; }
        }

        /// <summary>
        /// Gets or sets the droprate of the daimyo character.
        /// </summary>
        public int DropRate
        {
            get { return this.dropRate; }
            set { this.dropRate = value; }
        }

        /// <summary>
        /// Drops the daimyo character.
        /// </summary>
        public void Drop()
        {
            this.DY = 1;
            this.UpdateAreaY(this.DY * this.dropRate * 0.25);
        }

        /// <summary>
        /// Jumps the daimyo character.
        /// </summary>
        /// <param name="jumpticks">Multiplier for jump movement.</param>
        public void Jump(int jumpticks)
        {
            this.DY = -1;
            this.UpdateAreaY(this.DY * this.jumpRate * (jumpticks + 0.5));
        }
    }
}
