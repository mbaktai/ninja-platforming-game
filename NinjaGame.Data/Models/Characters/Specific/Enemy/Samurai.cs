﻿// <copyright file="Samurai.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents an enemy samurai character.
    /// </summary>
    public class Samurai : Character, IShoots
    {
        private DateTime shootLastUse;
        private int cooldownMS;

        /// <summary>
        /// Initializes a new instance of the <see cref="Samurai"/> class.
        /// </summary>
        public Samurai()
        {
            this.DX = 0;
            this.DY = 0;
            this.IsDead = false;
            this.cooldownMS = 6000;
        }

        /// <inheritdoc/>
        public int CooldownMS
        {
            get { return this.cooldownMS; }
        }

        /// <inheritdoc/>
        public DateTime ShootLastUse
        {
            get { return this.shootLastUse; }
            set { this.shootLastUse = value; }
        }
    }
}
