﻿// <copyright file="Archer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents an enemy archer character.
    /// </summary>
    public class Archer : Character, IShoots
    {
        private DateTime shootLastUse;
        private int cooldownMS;

        /// <summary>
        /// Initializes a new instance of the <see cref="Archer"/> class.
        /// </summary>
        public Archer()
        {
            this.DX = 0;
            this.DY = 0;
            this.IsDead = false;
            this.cooldownMS = 5000;
        }

        /// <inheritdoc/>
        public int CooldownMS
        {
            get { return this.cooldownMS; }
        }

        /// <inheritdoc/>
        public DateTime ShootLastUse
        {
            get { return this.shootLastUse; }
            set { this.shootLastUse = value; }
        }
    }
}
