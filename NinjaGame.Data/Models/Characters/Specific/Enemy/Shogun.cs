﻿// <copyright file="Shogun.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents an enemy shogun character.
    /// </summary>
    public class Shogun : Character
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Shogun"/> class.
        /// </summary>
        public Shogun()
        {
        }
    }
}
