﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents the player's character.
    /// </summary>
    public class Player : Character, IShoots
    {
        private int step;
        private int dropRate;
        private int jumpRate;
        private DateTime shootLastUse;
        private int cooldownMS;

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        public Player()
        {
            this.DX = 0;
            this.DY = 0;
            this.Step = 6;
            this.DropRate = 8;
            this.JumpRate = 16;
            this.IsDead = false;
            this.cooldownMS = 500;
        }

        /// <inheritdoc/>
        public DateTime ShootLastUse
        {
            get { return this.shootLastUse; }
            set { this.shootLastUse = value; }
        }

        /// <summary>
        /// Gets or sets the step rate for the player.
        /// </summary>
        public int Step
        {
            get { return this.step; }
            set { this.step = value; }
        }

        /// <summary>
        /// Gets or sets the drop rate for the player.
        /// </summary>
        public int DropRate
        {
            get { return this.dropRate; }
            set { this.dropRate = value; }
        }

        /// <summary>
        /// Gets or sets jump rate for the plaayer.
        /// </summary>
        public int JumpRate
        {
            get { return this.jumpRate; }
            set { this.jumpRate = value; }
        }

        /// <inheritdoc/>
        public int CooldownMS
        {
            get { return this.cooldownMS; }
        }

        /// <summary>
        /// Move the player upwards, animating a jump.
        /// </summary>
        /// <param name="ticks">The tick left for the jump movement.</param>
        public void Jump(int ticks)
        {
            this.UpdateAreaY(this.DY * this.JumpRate * ticks);
        }

        /// <summary>
        /// Moves the player to the left.
        /// </summary>
        /// <param name="leftBound">The left boundary of the gamespace.</param>
        /// <param name="rightBound">The Right boundary of the gamespace.</param>
        public void MoveLeft(double leftBound, double rightBound)
        {
            this.UpdateAreaX(this.DX * this.Step, leftBound, rightBound);
        }

        /// <summary>
        /// Moves the player to the right.
        /// </summary>
        /// /// <param name="leftBound">The left boundary of the gamespace.</param>
        /// <param name="rightBound">The Right boundary of the gamespace.</param>
        public void MoveRight(double leftBound, double rightBound)
        {
            this.UpdateAreaX(this.DX * this.Step, leftBound, rightBound);
        }

        /// <summary>
        /// Slides the player with the platform the player stands on.
        /// </summary>
        /// <param name="dx">The x direcion of the platform (-1 is left, 1 is right).</param>
        /// <param name="step">The step rate of the platform.</param>
        /// <param name="leftBound">The left boundary of the gamespace.</param>
        /// <param name="rightBound">The Right boundary of the gamespace.</param>
        public void Slide(double dx, double step, double leftBound, double rightBound)
        {
            this.DX = dx;
            this.UpdateAreaX(this.DX * step, leftBound, rightBound);
        }

        /// <summary>
        /// Drops the player down, gives it downwards motion.
        /// </summary>
        public void Fall()
        {
            this.UpdateAreaY(this.DY * this.dropRate);
        }
    }
}
