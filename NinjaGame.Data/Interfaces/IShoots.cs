﻿// <copyright file="IShoots.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines methods for characters capable of shooting projectiles.
    /// </summary>
    public interface IShoots
    {
        /// <summary>
        /// Gets or sets the shoting cooldown representation timestamp.
        /// </summary>
        DateTime ShootLastUse { get; set; }

        /// <summary>
        /// Gets the shoting cooldown representation in miliseconds.
        /// </summary>
        int CooldownMS { get; }
    }
}
