﻿// <copyright file="IDoesSomePickupStuff.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines methods for pick-ups.
    /// </summary>
    public interface IDoesSomePickupStuff
    {
        /// <summary>
        /// Represents a method for the pick-up to do its pick-up work (e.g.: healing).
        /// </summary>
        void DoPickup();
    }
}
