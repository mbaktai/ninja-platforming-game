﻿// <copyright file="IGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the methods available for the business logic.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Represents a method for jumping.
        /// </summary>
        void Jump();

        /// <summary>
        /// Represents a method for moving right.
        /// </summary>
        void MoveRight();

        /// <summary>
        /// Represents a method for moving left.
        /// </summary>
        void MoveLeft();

        /// <summary>
        /// Represents a method for droping down from a platform.
        /// </summary>
        void DropDown();

        /// <summary>
        /// Represents a method for shooting a projectile.
        /// </summary>
        void Shoot();

        /// <summary>
        /// Represents a method for the player shooting a projectile.
        /// </summary>
        void PlayerShoot();

        /// <summary>
        /// Represents a method for saving the current game state.
        /// </summary>
        void Save();

        /// <summary>
        /// Represents a method for saving the run's time.
        /// </summary>
        void SaveTime();

        /// <summary>
        /// Represents a method for loading a game state.
        /// </summary>
        void LoadGame();
    }
}
