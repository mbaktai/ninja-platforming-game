﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Forms;
    using System.Windows.Media.Imaging;
    using System.Xml.Linq;
    using NinjaGame.Data;
    using NinjaGame.Repository;

    /// <summary>
    /// Represents the business logic.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        private GameModel ninjaGameModel;
        private double leftBoundary;
        private double rightBoundary;
        private bool isPaused;
        private int jumpTicks;
        private double actWidth;
        private double actHeight;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="model">The game model to use.</param>
        /// <param name="actWidth">The gamespace's actual width.</param>
        /// <param name="actHeight">The gamespace's actual height.</param>
        public GameLogic(GameModel model, double actWidth, double actHeight)
        {
            this.ninjaGameModel = model;

            this.NinjaGameModel.CurrentLevel = "Level1";
            this.leftBoundary = 20;
            this.actWidth = actWidth;
            this.actHeight = actHeight;
            this.isPaused = true;

            this.jumpTicks = 0;

            this.LoadNextLevel();
        }

        /// <summary>
        /// Event for refreshing the screen.
        /// </summary>
        public event EventHandler RefreshScreen;

        /// <summary>
        /// Gets or sets the model for the logic.
        /// </summary>
        public GameModel NinjaGameModel
        {
            get { return this.ninjaGameModel; }
            set { this.ninjaGameModel = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the game is paused or not.
        /// </summary>
        public bool IsPaused
        {
            get { return this.isPaused; }
            set { this.isPaused = value; }
        }

        /// <summary>
        /// Gets or sets the ticks until the end of the jump movement.
        /// </summary>
        public int JumpTicks
        {
            get { return this.jumpTicks; }
            set { this.jumpTicks = value; }
        }

        /// <summary>
        /// Moves the projectiles.
        /// </summary>
        public void MoveProjectiles()
        {
            if (this.NinjaGameModel.Projectiles.Count > 0)
            {
                foreach (var actProj in this.NinjaGameModel.Projectiles.ToList())
                {
                    if (actProj.Area.Right <= this.actWidth && actProj.Area.Left >= this.leftBoundary - 10)
                    {
                        IEnumerable<Arrow> enemyArrows = this.NinjaGameModel.Projectiles.OfType<Arrow>().ToList();
                        IEnumerable<Sword> enemySwords = this.NinjaGameModel.Projectiles.OfType<Sword>().ToList();

                        if (actProj.GetType() == typeof(Shuriken))
                        {
                            bool hitEnemy = false;
                            bool hitProj = false;

                            foreach (var actArrow in enemyArrows)
                            {
                                if (actProj.Area.IntersectsWith(actArrow.Area) == true)
                                {
                                    hitProj = true;
                                    this.NinjaGameModel.Projectiles.Remove(actArrow);
                                    this.NinjaGameModel.Projectiles.Remove(actProj);
                                }
                            }

                            foreach (var actSword in enemySwords)
                            {
                                if (actProj.Area.IntersectsWith(actSword.Area) == true)
                                {
                                    hitProj = true;
                                    this.NinjaGameModel.Projectiles.Remove(actSword);
                                    this.NinjaGameModel.Projectiles.Remove(actProj);
                                }
                            }

                            foreach (var actEnemy in this.NinjaGameModel.CurrentEnemies.ToList())
                            {
                                if (actProj.Area.IntersectsWith(actEnemy.Area) == true)
                                {
                                    hitEnemy = true;

                                    actEnemy.Health -= actProj.Damage;
                                    this.NinjaGameModel.Projectiles.Remove(actProj);

                                    this.CheckCurrentEnemyDeath(actEnemy);
                                }
                            }

                            if (!hitEnemy && !hitProj)
                            {
                                actProj.Move(this.leftBoundary, this.rightBoundary);
                            }
                        }
                        else
                        {
                            bool hitPlayer = false;

                            if (actProj.Area.IntersectsWith(this.NinjaGameModel.ActualPlayer.Area) == true)
                            {
                                hitPlayer = true;

                                this.NinjaGameModel.ActualPlayer.Health -= actProj.Damage;
                                this.NinjaGameModel.Projectiles.Remove(actProj);

                                this.CheckEndofGame();
                            }

                            if (!hitPlayer)
                            {
                                actProj.Move(this.leftBoundary, this.rightBoundary);
                            }
                        }
                    }
                    else
                    {
                        this.NinjaGameModel.Projectiles.Remove(actProj);
                    }
                }
            }
        }

        /// <summary>
        /// Moves the platforms.
        /// </summary>
        public void MovePlatforms()
        {
            foreach (var actPlatform in this.ninjaGameModel.Platforms)
            {
                if (actPlatform.IsPlayerPlatform == true)
                {
                    actPlatform.Move(this.leftBoundary, this.rightBoundary);

                    if (actPlatform.Area.Left <= this.leftBoundary || actPlatform.Area.Right >= this.rightBoundary)
                    {
                        actPlatform.DX *= -1;
                    }

                    this.RefreshScreen?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        /// <inheritdoc/>
        public void Jump()
        {
            if (this.JumpTicks > 0)
            {
                this.NinjaGameModel.ActualPlayer.DY = -1;
                this.ninjaGameModel.ActualPlayer.Jump(this.JumpTicks);
                this.JumpTicks--;
            }
        }

        /// <inheritdoc/>
        public void MoveRight()
        {
            this.NinjaGameModel.ActualPlayer.DX = 1;

            if (this.ninjaGameModel.ActualPlayer.Area.Right + (this.NinjaGameModel.ActualPlayer.DX * this.ninjaGameModel.ActualPlayer.Step) <= this.rightBoundary)
            {
                this.ninjaGameModel.ActualPlayer.MoveRight(this.leftBoundary, this.rightBoundary);
                this.RefreshScreen?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Moves the last enemy, daimyo character accordingly to its position.
        /// </summary>
        public void MoveDaimyo()
        {
            // Első ellenőrzés
            if (this.NinjaGameModel.CurrentEnemies.First().DY == 0)
            {
                this.NinjaGameModel.CurrentEnemies.First().DY = -1;
                (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).JumpRate = 16;
                (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).DropRate = 16;

                this.NinjaGameModel.DaimyoJumpticks = 3;
                (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).Jump(this.NinjaGameModel.DaimyoJumpticks);
                (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).LastMove = DateTime.Now;
            }
            else
            {
                // Mozgatás

                // Legfelsőn
                if (this.DaimyoOnPlatform() == this.NinjaGameModel.Platforms.ElementAt(3) && this.NinjaGameModel.CurrentEnemies.First().DY == -1)
                {
                    if (DateTime.Now.Subtract((this.NinjaGameModel.CurrentEnemies.First() as Daimyo).LastMove).TotalMilliseconds >= 250)
                    {
                        (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).Drop();
                        (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).LastMove = DateTime.Now;
                    }
                }

                // Legalsón
                else if (this.DaimyoOnPlatform() == this.NinjaGameModel.Platforms.ElementAt(5) && this.NinjaGameModel.CurrentEnemies.First().DY == 1)
                {
                    if (DateTime.Now.Subtract((this.NinjaGameModel.CurrentEnemies.First() as Daimyo).LastMove).TotalMilliseconds >= 250)
                    {
                        this.NinjaGameModel.DaimyoJumpticks = 3;
                        (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).Jump(this.NinjaGameModel.DaimyoJumpticks);
                        (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).LastMove = DateTime.Now;
                    }
                }

                // Középsőn
                else if (this.DaimyoOnPlatform() == this.NinjaGameModel.Platforms.ElementAt(4))
                {
                    // Lefelé
                    if (this.NinjaGameModel.CurrentEnemies.First().DY == 1)
                    {
                        this.NinjaGameModel.CurrentEnemies.First().UpdateY(this.NinjaGameModel.Platforms.ElementAt(4).Area.Bottom + 22);
                        (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).Drop();
                        (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).LastMove = DateTime.Now;
                    }

                    // Felfelé
                    else if (this.NinjaGameModel.CurrentEnemies.First().DY == -1)
                    {
                        this.NinjaGameModel.CurrentEnemies.First().UpdateY(this.NinjaGameModel.Platforms.ElementAt(4).Area.Top - 1);
                        this.NinjaGameModel.DaimyoJumpticks = 3;
                        (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).Jump(this.NinjaGameModel.DaimyoJumpticks);
                        (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).LastMove = DateTime.Now;
                    }
                }
                else
                {
                    // Zuhan
                    if (this.NinjaGameModel.CurrentEnemies.First().DY == 1)
                    {
                        (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).Drop();
                    }

                    // Ugrik
                    else
                    {
                        if (this.NinjaGameModel.DaimyoJumpticks == 0)
                        {
                            (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).Drop();
                        }
                        else
                        {
                            this.NinjaGameModel.DaimyoJumpticks--;
                            (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).Jump(this.NinjaGameModel.DaimyoJumpticks);
                        }
                    }
                }
            }
        }

        /// <inheritdoc/>
        public void MoveLeft()
        {
            this.NinjaGameModel.ActualPlayer.DX = -1;

            if (this.ninjaGameModel.ActualPlayer.Area.Left + (this.NinjaGameModel.ActualPlayer.DX * this.ninjaGameModel.ActualPlayer.Step) >= this.leftBoundary)
            {
                this.ninjaGameModel.ActualPlayer.MoveLeft(this.leftBoundary, this.rightBoundary);
                this.RefreshScreen?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Slides the player with the platform the player stands on.
        /// </summary>
        /// <param name="dx">The x direcion of the platform (-1 is left, 1 is right).</param>
        /// <param name="step">The step rate of the platform.</param>
        public void SlidePlayer(double dx, int step)
        {
            this.NinjaGameModel.ActualPlayer.Slide(dx, step, this.leftBoundary, this.rightBoundary);
        }

        /// <summary>
        /// Moves the player downwards.
        /// </summary>
        public void FallPlayer()
        {
            this.NinjaGameModel.ActualPlayer.DY = 1;
            this.NinjaGameModel.ActualPlayer.Fall();

            this.CheckEndofGame();
        }

        /// <summary>
        /// Stops the player from falling.
        /// </summary>
        public void StopPlayerFall()
        {
            this.NinjaGameModel.ActualPlayer.DY = 0;
            this.NinjaGameModel.ActualPlayer.UpdateY(this.SameLevelForPlayerAndPlatform().Area.Top - 1);
        }

        /// <inheritdoc/>
        public void DropDown()
        {
            this.NinjaGameModel.ActualPlayer.DY = 1;
            this.NinjaGameModel.ActualPlayer.UpdateAreaY(1);
            this.NinjaGameModel.ActualPlayer.Fall();
        }

        /// <summary>
        /// Saves data regarding player.
        /// </summary>
        public void SaveGamePlayerData()
        {
            Player player = this.ninjaGameModel.ActualPlayer;
            DataReader.SavePlayerData(player.Health, player.Area.TopLeft.X, player.Area.TopLeft.Y, (int)player.DX, (int)player.DY);
        }

        /// <summary>
        /// Saves data regarding levels.
        /// </summary>
        public void SaveGameLevelData()
        {
            DataReader.SaveLevelData(
                this.ninjaGameModel.CurrentLevel,
                this.ninjaGameModel.LevelStopwatch.ElapsedMilliseconds.ToString(),
                this.ninjaGameModel.RemainingEnemies.Count,
                this.ninjaGameModel.CurrentEnemies.Count,
                this.ninjaGameModel.RemainingEnemies.Count(x => x.GetType() == typeof(Samurai)),
                this.ninjaGameModel.DaimyoJumpticks,
                this.jumpTicks);

            if (!File.Exists(@"..\..\Saves\Custom\SaveExample" + DateTime.Now.Year.ToString()
                + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".xml"))
            {
                string path = @"..\..\Saves\Custom\SaveExample" + DateTime.Now.Year.ToString()
                + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".xml";
                string crd = Directory.GetCurrentDirectory();

                File.Copy("SaveExample.xml", path);
            }
        }

        /// <summary>
        /// Saves data regarding platforms.
        /// </summary>
        public void SaveGamePlatformData()
        {
            Platform p1 = this.ninjaGameModel.Platforms.ElementAt(0);
            Platform p2 = this.ninjaGameModel.Platforms.ElementAt(1);
            Platform p3 = this.ninjaGameModel.Platforms.ElementAt(2);

            DataReader.SavePlatformData("Platform1", p1.Area.TopLeft.X, p1.Area.TopLeft.Y, (int)p1.DX);
            DataReader.SavePlatformData("Platform2", p2.Area.TopLeft.X, p2.Area.TopLeft.Y, (int)p2.DX);
            DataReader.SavePlatformData("Platform3", p3.Area.TopLeft.X, p3.Area.TopLeft.Y, (int)p3.DX);
        }

        /// <summary>
        /// Saves data regarding results.
        /// </summary>
        public void SaveGameResultsData()
        {
            string[] res = new string[this.ninjaGameModel.Results.Length];
            for (int i = 0; i < this.ninjaGameModel.Results.Length; i++)
            {
                if (this.ninjaGameModel.Results[i] != null)
                {
                    res[i] = this.ninjaGameModel.Results[i].TotalMilliseconds.ToString();
                }
            }

            DataReader.SaveResults(res);
        }

        /// <summary>
        /// Saves data regarding enemy characters.
        /// </summary>
        public void SaveGameEnemyData()
        {
            string[] enemyNames = { "Enemy1", "Enemy2", "Enemy3" };

            for (int i = 0; i < this.ninjaGameModel.CurrentEnemies.Count; i++)
            {
                Character ch = this.ninjaGameModel.CurrentEnemies.ElementAt(i);
                if (ch.GetType() == typeof(Samurai))
                {
                    DataReader.SaveEnemyData(enemyNames[i], "Samurai", ch.Health, ch.Area.TopLeft.X, ch.Area.TopLeft.Y);
                }
                else if (this.ninjaGameModel.CurrentEnemies.ElementAt(i).GetType() == typeof(Archer))
                {
                    DataReader.SaveEnemyData(enemyNames[i], "Archer", ch.Health, ch.Area.TopLeft.X, ch.Area.TopLeft.Y);
                }
                else
                {
                    DataReader.SaveEnemyData(enemyNames[i], "Daimyo", ch.Health, ch.Area.TopLeft.X, ch.Area.TopLeft.Y);
                }
            }
        }

        /// <inheritdoc/>
        public void LoadGame()
        {
            // Játék betöltése
            string saveFile;

            OpenFileDialog d = new OpenFileDialog();
            d.Filter = "TextFiles|*.xml";
            if (d.ShowDialog() == DialogResult.OK)
            {
                saveFile = d.FileName;

                // Valami ellenőrzés, jó-e a fájl, stb.
                if (saveFile != string.Empty && saveFile != null)
                {
                    this.NinjaGameModel.ActualPlayer.IsDead = false;

                    XDocument doc = XDocument.Load(saveFile);

                    this.jumpTicks = int.Parse((from x in doc.Descendants("SavedGame")
                                                select x.Element("JumpTicks").Value).Single());

                    this.NinjaGameModel.DaimyoJumpticks = int.Parse((from x in doc.Descendants("SavedGame")
                                                                     select x.Element("DaimyoJumpTicks").Value).Single());

                    this.ninjaGameModel.LevelStopwatch.Reset();

                    this.NinjaGameModel.CurrentEnemies.Clear();
                    this.NinjaGameModel.RemainingEnemies.Clear();
                    this.NinjaGameModel.Projectiles.Clear();
                    this.NinjaGameModel.Platforms.Clear();

                    // Pálya
                    this.NinjaGameModel.CurrentLevel = (from x in doc.Descendants("SavedGame")
                                                         select x.Element("Level").Value).Single();

                    // Player
                    BitmapImage myBitmapImage = new BitmapImage(new Uri(@"..\..\_player.bmp", UriKind.RelativeOrAbsolute));

                    this.NinjaGameModel.GameRepository.GetAGameObject("Player");
                    this.NinjaGameModel.ActualPlayer.Health = int.Parse((from x in doc.Descendants("SavedGame").
                                                                         Descendants("Characters").
                                                                         Descendants("Player")
                                                               select x.Element("Health").Value).Single());
                    this.ninjaGameModel.ActualPlayer.DX = int.Parse((from x in doc.Descendants("SavedGame").
                                                                         Descendants("Characters").
                                                                         Descendants("Player")
                                                                     select x.Element("DX").Value).Single());
                    this.NinjaGameModel.ActualPlayer.DY = int.Parse((from x in doc.Descendants("SavedGame").
                                                                         Descendants("Characters").
                                                                         Descendants("Player")
                                                                     select x.Element("DY").Value).Single());
                    double posX = double.Parse((from x in doc.Descendants("SavedGame").
                                                                         Descendants("Characters").
                                                                         Descendants("Player")
                                       select x.Element("X").Value).Single().Replace('.', ','));
                    double posY = double.Parse((from x in doc.Descendants("SavedGame").
                                                                         Descendants("Characters").
                                                                         Descendants("Player")
                                                select x.Element("Y").Value).Single().Replace('.', ','));
                    this.NinjaGameModel.ActualPlayer.Area = new Rect(posX, posY, myBitmapImage.Width, myBitmapImage.Height);
                    this.NinjaGameModel.ActualPlayer.ShootLastUse = DateTime.Now;

                    // TESZT ----------------------------------------------------------------------------------------------------
                    this.NinjaGameModel.ActualPlayer.Damage = 100;

                    if (int.Parse(this.NinjaGameModel.CurrentLevel.Last().ToString()) > 1)
                    {
                        this.NinjaGameModel.ActualPlayer.DropRate = 12;
                        this.ninjaGameModel.ActualPlayer.JumpRate = 18;
                        if (int.Parse(this.NinjaGameModel.CurrentLevel.Last().ToString()) > 3)
                        {
                            this.NinjaGameModel.ActualPlayer.DropRate = 18;
                            this.ninjaGameModel.ActualPlayer.JumpRate = 24;
                        }
                    }

                    // Platformok
                    myBitmapImage = new BitmapImage(new Uri(@"..\..\_platform.bmp", UriKind.RelativeOrAbsolute));

                    for (int i = 0; i < 6; i++)
                    {
                        Platform platf = (Platform)this.ninjaGameModel.GameRepository.GetAGameObject("Platform");

                        // Horizontals.
                        if (i < 6)
                        {
                            if (i < 3)
                            {
                                platf.IsPlayerPlatform = true;

                                // Platf.step beállítása XML szerint
                                platf.Step = 4 * this.NinjaGameModel.GameRepository.ReadPlatformSpeed(this.NinjaGameModel.CurrentLevel);

                                if (i == 1)
                                {
                                    platf.DX *= -1;
                                }
                            }
                            else
                            {
                                platf.Step = 0;
                            }

                            platf.IsHorizontal = true;
                        }

                        this.ninjaGameModel.Platforms.Add(platf);
                    }

                    int j = 1;

                    this.rightBoundary = 960 - myBitmapImage.Width - 60;

                    foreach (var actPlatf in this.ninjaGameModel.Platforms)
                    {
                        int dx = 2;

                        if (j < 4)
                        {
                            posX = double.Parse((from x in doc.Descendants("SavedGame").
                                                                         Descendants("Platforms").
                                                                         Descendants("Platform" + j.ToString())
                                                 select x.Element("X").Value).Single().Replace('.', ','));
                            posY = double.Parse((from x in doc.Descendants("SavedGame").
                                                                                 Descendants("Platforms").
                                                                                 Descendants("Platform" + j.ToString())
                                                 select x.Element("Y").Value).Single().Replace('.', ','));

                            dx = int.Parse((from x in doc.Descendants("SavedGame").
                                                                         Descendants("Platforms").
                                                                         Descendants("Platform" + j.ToString())
                                                select x.Element("DX").Value).Single());
                        }

                        switch (j)
                        {
                            case 1:
                                actPlatf.Area = new Rect(posX, posY, myBitmapImage.Width, myBitmapImage.Height);
                                actPlatf.DX = dx;
                                j++;
                                break;
                            case 2:
                                actPlatf.Area = new Rect(posX, posY, myBitmapImage.Width, myBitmapImage.Height);
                                actPlatf.DX = dx;
                                j++;
                                break;
                            case 3:
                                actPlatf.Area = new Rect(posX, posY, myBitmapImage.Width, myBitmapImage.Height);
                                actPlatf.DX = dx;
                                j++;
                                break;
                            case 4:
                                actPlatf.Area = new Rect(960 - myBitmapImage.Width - 20, 150, myBitmapImage.Width, myBitmapImage.Height); j++;
                                break;
                            case 5:
                                actPlatf.Area = new Rect(960 - myBitmapImage.Width - 20, 300, myBitmapImage.Width, myBitmapImage.Height); j++;
                                break;
                            case 6:
                                actPlatf.Area = new Rect(960 - myBitmapImage.Width - 20, 450, myBitmapImage.Width, myBitmapImage.Height); j++;
                                break;
                            default:
                                actPlatf.Area = new Rect(0, 0, myBitmapImage.Width, myBitmapImage.Height); j++;
                                break;
                        }
                    }

                    // Ellenségek
                    myBitmapImage = new BitmapImage(new Uri(@"..\..\_archer.bmp", UriKind.RelativeOrAbsolute));

                    int noOfSamurai = int.Parse((from x in doc.Descendants("SavedGame")
                                                 select x.Element("RemainingSamurai").Value).Single());
                    int noOfArcher = int.Parse((from x in doc.Descendants("SavedGame")
                                                select x.Element("RemainingEnemy").Value).Single()) -
                                                int.Parse((from x in doc.Descendants("SavedGame")
                                                           select x.Element("RemainingSamurai").Value).Single()) - 1;

                    // Archerek
                    for (int i = 0; i < noOfArcher; i++)
                    {
                        Archer archer = (Archer)this.NinjaGameModel.GameRepository.GetAGameObject("Archer");
                        archer.Area = new Rect(
                            this.NinjaGameModel.Platforms.Last().Area.Left + 50,
                            this.NinjaGameModel.Platforms.Last().Area.Top - myBitmapImage.Height - 1,
                            myBitmapImage.Width,
                            myBitmapImage.Height);
                        archer.ShootLastUse = DateTime.Now;

                        this.NinjaGameModel.RemainingEnemies.Add(archer);
                    }

                    myBitmapImage = new BitmapImage(new Uri(@"..\..\_samurai.bmp", UriKind.RelativeOrAbsolute));
                    for (int i = 0; i < noOfSamurai; i++)
                    {
                        Samurai samurai = (Samurai)this.NinjaGameModel.GameRepository.GetAGameObject("Samurai");
                        samurai.Area = new Rect(
                            this.NinjaGameModel.Platforms.Last().Area.Left + 50,
                            this.NinjaGameModel.Platforms.Last().Area.Top - myBitmapImage.Height - 1,
                            myBitmapImage.Width,
                            myBitmapImage.Height);
                        samurai.DX = 0;
                        samurai.DY = 0;
                        samurai.IsDead = false;
                        samurai.ShootLastUse = DateTime.Now;

                        this.NinjaGameModel.RemainingEnemies.Add(samurai);
                    }

                    // Keverés
                    this.ShuffleObservable(this.NinjaGameModel.RemainingEnemies);

                    if (this.NinjaGameModel.RemainingEnemies.Count != 0)
                    {
                        // Daimyo
                        myBitmapImage = new BitmapImage(new Uri(@"..\..\_daimyo.bmp", UriKind.RelativeOrAbsolute));
                        Daimyo daimyo = (Daimyo)this.NinjaGameModel.GameRepository.GetAGameObject("Daimyo");
                        daimyo.Area = new Rect(
                            this.NinjaGameModel.Platforms.Last().Area.Left + 50,
                            this.NinjaGameModel.Platforms.Last().Area.Top - myBitmapImage.Height - 1,
                            myBitmapImage.Width,
                            myBitmapImage.Height);
                        daimyo.DX = 0;
                        daimyo.DY = 0;
                        daimyo.IsDead = false;

                        this.NinjaGameModel.RemainingEnemies.Add(daimyo);
                    }

                    // Pillanatnyi ellenségek megadása
                    int enemiesNow = int.Parse((from x in doc.Descendants("SavedGame")
                                                      select x.Element("CurrentEnemyNo").Value).Single());

                    for (int i = 0; i < enemiesNow; i++)
                    {
                        string currentType = (from x in doc.Descendants("SavedGame").
                                                                         Descendants("Characters").
                                                                         Descendants("Enemy" + (i + 1).ToString())
                                                           select x.Element("Type").Value).Single();

                        this.NinjaGameModel.CurrentEnemies.Add((Character)this.NinjaGameModel.GameRepository.GetAGameObject(currentType));
                        posX = double.Parse((from x in doc.Descendants("SavedGame").
                                                                         Descendants("Characters").
                                                                         Descendants("Enemy" + (i + 1).ToString())
                                             select x.Element("X").Value).Single().Replace('.', ','));
                        posY = double.Parse((from x in doc.Descendants("SavedGame").
                                                                     Descendants("Characters").
                                                                     Descendants("Enemy" + (i + 1).ToString())
                                             select x.Element("Y").Value).Single().Replace('.', ','));

                        int health = int.Parse((from x in doc.Descendants("SavedGame").
                                                                         Descendants("Characters").
                                                                         Descendants("Enemy" + (i + 1).ToString())
                                                select x.Element("Health").Value).Single());
                        if (i == 0)
                        {
                            this.NinjaGameModel.CurrentEnemies.First().Health = health;
                            string picname = "_" + this.NinjaGameModel.CurrentEnemies.First().GetType().Name.ToLower() + ".bmp";
                            myBitmapImage = new BitmapImage(new Uri(@"..\..\" + picname, UriKind.RelativeOrAbsolute));

                            this.NinjaGameModel.CurrentEnemies.First().Area = new Rect(posX, posY, myBitmapImage.Width, myBitmapImage.Height);
                            if (this.NinjaGameModel.CurrentEnemies.First().GetType() != typeof(Daimyo))
                            {
                                (this.ninjaGameModel.CurrentEnemies.First() as IShoots).ShootLastUse = DateTime.Now;
                            }
                            else
                            {
                                Random r = new Random();
                                (this.NinjaGameModel.CurrentEnemies.First() as Daimyo).DY = r.Next(-1, 1);
                            }
                        }
                        else if (i == 1)
                        {
                            this.NinjaGameModel.CurrentEnemies.ElementAt(i).Health = health;
                            string picname = "_" + this.NinjaGameModel.CurrentEnemies.First().GetType().Name.ToLower() + ".bmp";
                            myBitmapImage = new BitmapImage(new Uri(@"..\..\" + picname, UriKind.RelativeOrAbsolute));

                            this.NinjaGameModel.CurrentEnemies.ElementAt(i).Area = new Rect(posX, posY, myBitmapImage.Width, myBitmapImage.Height);
                            (this.ninjaGameModel.CurrentEnemies.ElementAt(i) as IShoots).ShootLastUse = DateTime.Now;
                        }
                        else if (i == 2)
                        {
                            this.NinjaGameModel.CurrentEnemies.ElementAt(i).Health = health;
                            string picname = "_" + this.NinjaGameModel.CurrentEnemies.First().GetType().Name.ToLower() + ".bmp";
                            myBitmapImage = new BitmapImage(new Uri(@"..\..\" + picname, UriKind.RelativeOrAbsolute));

                            this.NinjaGameModel.CurrentEnemies.ElementAt(i).Area = new Rect(posX, posY, myBitmapImage.Width, myBitmapImage.Height);
                            (this.ninjaGameModel.CurrentEnemies.ElementAt(i) as IShoots).ShootLastUse = DateTime.Now;
                        }
                    }

                    // Pillanatnyi ellenségek platformra rakása
                    for (int i = 0; i < this.NinjaGameModel.CurrentEnemies.Count; i++)
                    {
                        this.NinjaGameModel.CurrentEnemies.ElementAt(i).UpdateY(this.NinjaGameModel.Platforms.ElementAt(i + 3).Area.Top - 1);
                        this.ninjaGameModel.CurrentEnemies.ElementAt(i).UpdateX((this.NinjaGameModel.Platforms.ElementAt(i + 3).Area.Left + this.NinjaGameModel.Platforms.ElementAt(i + 3).Area.Right) / 2);
                    }

                    // Eredmények stringben
                    string[] res = new string[5];
                    int idx = 0;
                    var q = from x in doc.Descendants("Results").Descendants()
                            select x;

                    foreach (var item in q)
                    {
                        res[idx++] = item.Value.ToString();
                    }

                    // Timespan-be
                    for (int i = 0; i < this.NinjaGameModel.Results.Length; i++)
                    {
                        TimeSpan temp = default(TimeSpan);

                        if (TimeSpan.TryParse(res[i], out temp) == true)
                        {
                            this.NinjaGameModel.Results[i] = temp;
                        }
                    }

                    this.RefreshScreen?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Loads the specified levedata into the model.
        /// </summary>
        public void LoadNextLevel()
        {
            this.NinjaGameModel.LevelStopwatch.Reset();

            this.NinjaGameModel.CurrentEnemies.Clear();
            this.NinjaGameModel.RemainingEnemies.Clear();
            this.NinjaGameModel.Projectiles.Clear();
            this.NinjaGameModel.Platforms.Clear();

            // Feltöltés
            for (int i = 0; i < 6; i++)
            {
                Platform platf = (Platform)this.ninjaGameModel.GameRepository.GetAGameObject("Platform");

                // Horizontals.
                if (i < 6)
                {
                    if (i < 3)
                    {
                        platf.IsPlayerPlatform = true;

                        // Platf.step beállítása XML szerint
                        platf.Step = 4 * this.NinjaGameModel.GameRepository.ReadPlatformSpeed(this.NinjaGameModel.CurrentLevel);

                        if (i == 1)
                        {
                            platf.DX *= -1;
                        }
                    }
                    else
                    {
                        platf.Step = 0;
                    }

                    platf.IsHorizontal = true;
                }

                this.ninjaGameModel.Platforms.Add(platf);
            }

            // Konvertálás/feltöltés
            int j = 1;

            BitmapImage myBitmapImage = new BitmapImage(new Uri(@"..\..\_platform.bmp", UriKind.RelativeOrAbsolute));

            this.rightBoundary = 960 - myBitmapImage.Width - 60;

            foreach (var actPlatf in this.ninjaGameModel.Platforms)
            {
                switch (j)
                {
                    case 1:
                        actPlatf.Area = new Rect(20, 150, myBitmapImage.Width, myBitmapImage.Height); j++;
                        break;
                    case 2:
                        actPlatf.Area = new Rect(320, 300, myBitmapImage.Width, myBitmapImage.Height); j++;
                        break;
                    case 3:
                        actPlatf.Area = new Rect(20, 450, myBitmapImage.Width, myBitmapImage.Height); j++;
                        break;
                    case 4:
                        actPlatf.Area = new Rect(960 - myBitmapImage.Width - 20, 150, myBitmapImage.Width, myBitmapImage.Height); j++;
                        break;
                    case 5:
                        actPlatf.Area = new Rect(960 - myBitmapImage.Width - 20, 300, myBitmapImage.Width, myBitmapImage.Height); j++;
                        break;
                    case 6:
                        actPlatf.Area = new Rect(960 - myBitmapImage.Width - 20, 450, myBitmapImage.Width, myBitmapImage.Height); j++;
                        break;
                    default:
                        actPlatf.Area = new Rect(0, 0, myBitmapImage.Width, myBitmapImage.Height); j++;
                        break;
                }
            }

            // PLAYER betöltése
            myBitmapImage = new BitmapImage(new Uri(@"..\..\_player.bmp", UriKind.RelativeOrAbsolute));
            this.ninjaGameModel.ActualPlayer = (Player)this.ninjaGameModel.GameRepository.GetAGameObject("Player");

            if (int.Parse(this.NinjaGameModel.CurrentLevel.Last().ToString()) > 1)
            {
                this.NinjaGameModel.ActualPlayer.DropRate = 12;
                this.ninjaGameModel.ActualPlayer.JumpRate = 18;
                if (int.Parse(this.NinjaGameModel.CurrentLevel.Last().ToString()) > 3)
                {
                    this.NinjaGameModel.ActualPlayer.DropRate = 18;
                    this.ninjaGameModel.ActualPlayer.JumpRate = 24;
                }
            }

            this.ninjaGameModel.ActualPlayer.Area = new Rect(
                this.ninjaGameModel.Platforms.First().Area.Left + 10,
                this.ninjaGameModel.Platforms.First().Area.Top - myBitmapImage.Height - 1,
                myBitmapImage.Width,
                myBitmapImage.Height);

            this.NinjaGameModel.ActualPlayer.ShootLastUse = DateTime.Now;

            // TESZT -------------------------------------------------------------------------------------------------------------------------------
            this.NinjaGameModel.ActualPlayer.Damage = 100;

            // TESZT -------------------------------------------------------------------------------------------------------------------------------

            // ELLENSÉGEK betöltése
            myBitmapImage = new BitmapImage(new Uri(@"..\..\_archer.bmp", UriKind.RelativeOrAbsolute));

            int noOfSamurai = this.ninjaGameModel.GameRepository.ReadNumberOfSamuraiForLevel(this.ninjaGameModel.CurrentLevel);
            int noOfArcher = this.NinjaGameModel.GameRepository.ReadNumberOfEnemiesForLevel(this.NinjaGameModel.CurrentLevel) - noOfSamurai;

            // Archerek
            for (int i = 0; i < noOfArcher; i++)
            {
                Archer archer = (Archer)this.NinjaGameModel.GameRepository.GetAGameObject("Archer");
                archer.Area = new Rect(
                    this.NinjaGameModel.Platforms.Last().Area.Left + 50,
                    this.NinjaGameModel.Platforms.Last().Area.Top - myBitmapImage.Height - 1,
                    myBitmapImage.Width,
                    myBitmapImage.Height);
                archer.ShootLastUse = DateTime.Now;

                this.NinjaGameModel.RemainingEnemies.Add(archer);
            }

            // Samurai-ok
            myBitmapImage = new BitmapImage(new Uri(@"..\..\_samurai.bmp", UriKind.RelativeOrAbsolute));
            for (int i = 0; i < noOfSamurai; i++)
            {
                Samurai samurai = (Samurai)this.NinjaGameModel.GameRepository.GetAGameObject("Samurai");
                samurai.Area = new Rect(
                    this.NinjaGameModel.Platforms.Last().Area.Left + 50,
                    this.NinjaGameModel.Platforms.Last().Area.Top - myBitmapImage.Height - 1,
                    myBitmapImage.Width,
                    myBitmapImage.Height);
                samurai.DX = 0;
                samurai.DY = 0;
                samurai.IsDead = false;
                samurai.ShootLastUse = DateTime.Now;

                this.NinjaGameModel.RemainingEnemies.Add(samurai);
            }

            // Keverés
            this.ShuffleObservable(this.NinjaGameModel.RemainingEnemies);

            // Daimyo
            myBitmapImage = new BitmapImage(new Uri(@"..\..\_daimyo.bmp", UriKind.RelativeOrAbsolute));
            Daimyo daimyo = (Daimyo)this.NinjaGameModel.GameRepository.GetAGameObject("Daimyo");
            daimyo.Area = new Rect(
                this.NinjaGameModel.Platforms.Last().Area.Left + 50,
                this.NinjaGameModel.Platforms.Last().Area.Top - myBitmapImage.Height - 1,
                myBitmapImage.Width,
                myBitmapImage.Height);
            daimyo.DX = 0;
            daimyo.DY = 0;
            daimyo.IsDead = false;

            this.NinjaGameModel.RemainingEnemies.Add(daimyo);

            // Pillanatnyi ellenségek megadása
            for (int i = 0; i < 3; i++)
            {
                this.NinjaGameModel.CurrentEnemies.Add(this.NinjaGameModel.RemainingEnemies.First());
                this.NinjaGameModel.RemainingEnemies.Remove(this.NinjaGameModel.RemainingEnemies.First());
            }

            // Pillanatnyi ellenségek platformra rakása
            for (int i = 0; i < 3; i++)
            {
                this.NinjaGameModel.CurrentEnemies.ElementAt(i).UpdateY(this.NinjaGameModel.Platforms.ElementAt(i + 3).Area.Top - 1);
                this.ninjaGameModel.CurrentEnemies.ElementAt(i).UpdateX((this.NinjaGameModel.Platforms.ElementAt(i + 3).Area.Left + this.NinjaGameModel.Platforms.ElementAt(i + 3).Area.Right) / 2);
            }

            if (this.NinjaGameModel.CurrentLevel != "Level1")
            {
                this.NinjaGameModel.LevelStopwatch.Start();
            }
        }

        /// <summary>
        /// Determines if the player is standing on a platform or not.
        /// </summary>
        /// <returns>A platform for standing on one, null for not.</returns>
        public Platform PlayerOnPlatform()
        {
            BitmapImage myBitmapImage = new BitmapImage();
            myBitmapImage.BeginInit();
            myBitmapImage.UriSource = new Uri(@"..\..\_player.bmp", UriKind.RelativeOrAbsolute);
            myBitmapImage.EndInit();

            Platform onPlatf = this.SameLevelForPlayerAndPlatform();

            if (this.SameLevelForPlayerAndPlatform() != null &&
                this.NinjaGameModel.ActualPlayer.Area.Right <= (onPlatf.Area.Right + myBitmapImage.Width) &&
                this.ninjaGameModel.ActualPlayer.Area.Left >= (onPlatf.Area.Left - myBitmapImage.Width))
            {
                return onPlatf;
            }

            return null;
        }

        /// <inheritdoc/>
        public void Shoot()
        {
            if (this.NinjaGameModel.CurrentEnemies.OfType<Archer>().Any() == true || this.NinjaGameModel.CurrentEnemies.OfType<Samurai>().Any() == true)
            {
                foreach (var actEnemy in this.NinjaGameModel.CurrentEnemies)
                {
                    this.EnemyShoot(actEnemy);
                }
            }
        }

        /// <inheritdoc/>
        public void PlayerShoot()
        {
            this.NinjaGameModel.ActualPlayer.ShootLastUse = DateTime.Now;

            Shuriken shrk = (Shuriken)this.NinjaGameModel.GameRepository.GetAGameObject("Shuriken");
            shrk.Damage = this.NinjaGameModel.ActualPlayer.Damage;
            shrk.UpdateX(this.NinjaGameModel.ActualPlayer.Area.Right + 1);
            shrk.UpdateY(this.NinjaGameModel.ActualPlayer.Area.Bottom - (this.NinjaGameModel.ActualPlayer.Area.Height / 2) + (shrk.Area.Height / 2));

            this.NinjaGameModel.Projectiles.Add(shrk);
        }

        /// <inheritdoc/>
        public void Save()
        {
            if (this.NinjaGameModel.RemainingEnemies.Count != 0)
            {
                this.SaveGameLevelData();
                this.SaveGamePlatformData();
                this.SaveGamePlayerData();
                this.SaveGameResultsData();
                this.SaveGameEnemyData();
            }
            else
            {
                this.isPaused = true;
                MessageBox.Show("Nincs csalás!", "NO CHEAT!", MessageBoxButtons.OK);
            }

            // DataReader.XDoc.Save("SaveExample.xml");
        }

        /// <inheritdoc/>
        public void SaveTime()
        {
            string[] levels =
           {
                "Level1", "Level2", "Level3", "Level4", "Level5"
            };

            int i = 0;

            while (i < levels.Length)
            {
                List<TimeSpan> savedTimes = DataReader.ReadLevelHighscores(levels[i]);

                bool saveValue = false;

                if (this.NinjaGameModel.Results[i] < savedTimes.ElementAt(0))
                {
                    // Első
                    savedTimes.Insert(0, this.ninjaGameModel.Results[i]);
                    savedTimes.Remove(savedTimes.Last());
                    saveValue = true;
                }
                else if (this.NinjaGameModel.Results[i] > savedTimes.ElementAt(0)
                    && this.NinjaGameModel.Results[i] < savedTimes.ElementAt(1))
                {
                    // Második
                    savedTimes.Insert(1, this.ninjaGameModel.Results[i]);
                    savedTimes.Remove(savedTimes.Last());
                    saveValue = true;
                }
                else if (this.NinjaGameModel.Results[i] > savedTimes.ElementAt(1)
                    && this.NinjaGameModel.Results[i] < savedTimes.ElementAt(2))
                {
                    // Harmadik
                    savedTimes.Insert(2, this.ninjaGameModel.Results[i]);
                    savedTimes.Remove(savedTimes.Last());
                    saveValue = true;
                }

                if (saveValue == true)
                {
                    DataReader.SaveTimeToXml(levels[i], savedTimes);
                }

                i++;
            }
        }

        // TESZT

        /// <summary>
        /// Places the player on the next level.
        /// </summary>
        public void IncreaseLevel()
        {
            char lvlChar = this.NinjaGameModel.CurrentLevel.Last();
            this.NinjaGameModel.CurrentLevel = "Level" + (int.Parse(lvlChar.ToString()) + 1).ToString();
            if (this.NinjaGameModel.CurrentLevel == "Level6")
            {
                this.CheckEndofGame();
            }
            else
            {
                this.LoadNextLevel();
            }
        }

        private void SaveTimeToArray()
        {
            char lvlChar = this.NinjaGameModel.CurrentLevel.Last();
            this.NinjaGameModel.Results[int.Parse(lvlChar.ToString()) - 1] = this.NinjaGameModel.LevelStopwatch.Elapsed;
        }

        private Platform DaimyoOnPlatform()
        {
            foreach (var actPlatf in this.NinjaGameModel.Platforms.Where(x => x.IsPlayerPlatform == false).ToList())
            {
                if (actPlatf.Area.IntersectsWith(new Rect(
                    this.NinjaGameModel.CurrentEnemies.First().Area.Left,
                    this.NinjaGameModel.CurrentEnemies.First().Area.Bottom - 20,
                    this.NinjaGameModel.CurrentEnemies.First().Area.Width,
                    20)))
                {
                    return actPlatf;
                }
            }

            return null;
        }

        private void EnemyShoot(Character enemyChar)
        {
            if (enemyChar.GetType() == typeof(Archer))
            {
                if (DateTime.Now.Subtract((enemyChar as Archer).ShootLastUse).TotalMilliseconds >= (enemyChar as Archer).CooldownMS)
                {
                    (enemyChar as Archer).ShootLastUse = DateTime.Now;

                    Arrow arw = (Arrow)this.NinjaGameModel.GameRepository.GetAGameObject("Arrow");
                    arw.Damage = enemyChar.Damage;
                    arw.UpdateX(enemyChar.Area.Left - arw.Area.Width - 1);
                    arw.UpdateY(enemyChar.Area.Bottom - (enemyChar.Area.Height / 2) + (arw.Area.Height / 2));

                    this.NinjaGameModel.Projectiles.Add(arw);
                }
            }
            else if (enemyChar.GetType() == typeof(Samurai))
            {
                if (DateTime.Now.Subtract((enemyChar as Samurai).ShootLastUse).TotalMilliseconds >= (enemyChar as Samurai).CooldownMS)
                {
                    (enemyChar as Samurai).ShootLastUse = DateTime.Now;

                    Sword swrd = (Sword)this.NinjaGameModel.GameRepository.GetAGameObject("Sword");
                    swrd.Damage = enemyChar.Damage;
                    swrd.UpdateX(enemyChar.Area.Left - swrd.Area.Width - 1);
                    swrd.UpdateY(enemyChar.Area.Bottom - (enemyChar.Area.Height / 2) + (swrd.Area.Height / 2) + 20);

                    this.NinjaGameModel.Projectiles.Add(swrd);
                }
            }
            else
            {
                return;
            }
        }

        private Platform SameLevelForPlayerAndPlatform()
        {
            foreach (var actPlatf in this.NinjaGameModel.Platforms)
            {
                if (Enumerable.Range(0, 10).Contains(
                        (int)Math.Round(actPlatf.Area.Top - this.NinjaGameModel.ActualPlayer.Area.Bottom)))
                {
                    return actPlatf;
                }
            }

            return null;
        }

        private void CheckEndofGame()
        {
            if (this.NinjaGameModel.ActualPlayer.Health <= 0 || this.NinjaGameModel.ActualPlayer.Area.Bottom >= this.actHeight)
            {
                this.NinjaGameModel.ActualPlayer.IsDead = true;
                this.IsPaused = true;
                MessageBox.Show("Meghaltál!", "GAME OVER", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (this.NinjaGameModel.CurrentLevel == "Level6")
            {
                this.IsPaused = true;
                this.NinjaGameModel.GameFinsihed = true;
                this.SaveTime();

                this.NinjaGameModel.CurrentLevel = "Level5";

                 MessageBox.Show(
                     "Nyertél!\r\nIdőid:\r\nLevel1: " + this.NinjaGameModel.Results[0].ToString(@"mm\:ss\.fff") +
                     "\r\nLevel2: " + this.NinjaGameModel.Results[1].ToString(@"mm\:ss\.fff") +
                    "\r\nLevel3: " + this.NinjaGameModel.Results[2].ToString(@"mm\:ss\.fff") +
                     "\r\nLevel4: " + this.NinjaGameModel.Results[3].ToString(@"mm\:ss\.fff") +
                     "\r\nLevel5: " + this.NinjaGameModel.Results[4].ToString(@"mm\:ss\.fff"), "GRATULÁLUNK",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
            }
        }

        private void CheckCurrentEnemyDeath(Character enemyChar)
        {
            if (enemyChar.Health <= 0)
            {
                Character temp = enemyChar;

                this.NinjaGameModel.CurrentEnemies.Remove(enemyChar);

                // Nincs több ellenség
                if (this.NinjaGameModel.CurrentEnemies.Count() == 0 && this.NinjaGameModel.RemainingEnemies.Count() == 0)
                {
                    // Idő elmentése
                    this.NinjaGameModel.LevelStopwatch.Stop();

                    this.SaveTimeToArray();
                    this.IncreaseLevel();
                    this.LoadNextLevel();
                }

                // Utolsó ellenség
                else if (this.NinjaGameModel.CurrentEnemies.Count() == 0 && this.NinjaGameModel.RemainingEnemies.First().GetType() == typeof(Daimyo))
                {
                    // Daimyo berakása
                    this.NinjaGameModel.RemainingEnemies.First().UpdateX(this.NinjaGameModel.Platforms.Last().Area.Right -
                        this.NinjaGameModel.RemainingEnemies.First().Area.Width - 10);
                    this.NinjaGameModel.RemainingEnemies.First().UpdateY(this.NinjaGameModel.Platforms.Last().Area.Top - 1);

                    this.NinjaGameModel.CurrentEnemies.Add(this.NinjaGameModel.RemainingEnemies.First());
                    this.NinjaGameModel.RemainingEnemies.Remove(this.NinjaGameModel.RemainingEnemies.First());
                }
                else if (this.NinjaGameModel.RemainingEnemies.First().GetType() != typeof(Daimyo))
                {
                    this.NinjaGameModel.RemainingEnemies.First().UpdateX(temp.Area.Left);
                    this.NinjaGameModel.RemainingEnemies.First().UpdateY(temp.Area.Bottom);

                    this.NinjaGameModel.CurrentEnemies.Add(this.NinjaGameModel.RemainingEnemies.First());
                    this.NinjaGameModel.RemainingEnemies.Remove(this.NinjaGameModel.RemainingEnemies.First());
                }
            }
        }

        private void ShuffleObservable<T>(ObservableCollection<T> target)
        {
            Random rng = new Random();
            int n = target.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = target[k];
                target[k] = target[n];
                target[n] = value;
            }
        }
    }
}
