﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using NinjaGame.Data;

    /// <summary>
    /// Represents the current state of the game.
    /// </summary>
    public class GameModel : IGameModel, INotifyPropertyChanged
    {
        private readonly IRepository gameRepository;

        private Player actualPlayer;
        private List<Character> currentEnemies;
        private List<Projectile> projectiles;
        private ObservableCollection<Character> remainingEnemies;
        private List<Platform> platforms;
        private string currentLevel;
        private bool gameFinished;
        private int daimyoJumpticks;
        private TimeSpan[] results;
        private Stopwatch levelStopwatch;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        public GameModel()
        {
            // Initialize repository.
            this.gameRepository = new GameRepository();
            this.CurrentEnemies = new List<Character>();
            this.RemainingEnemies = new ObservableCollection<Character>();
            this.Projectiles = new List<Projectile>();
            this.Platforms = new List<Platform>();
            this.gameFinished = false;
            this.DaimyoJumpticks = 0;
            this.results = new TimeSpan[5];
            this.levelStopwatch = new Stopwatch();
        }

        /// <summary>
        /// Event for when a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the number of remaining enemies.
        /// </summary>
        public int EnemiesLeft
        {
            get { return this.remainingEnemies.Count; }
            set { this.OnPropertyChanged(); }
        }

        /// <summary>
        /// Gets or sets the stopwatch (e.g. start, or stop) that runs on each level.
        /// </summary>
        public Stopwatch LevelStopwatch
        {
            get { return this.levelStopwatch; }
            set { this.levelStopwatch = value; }
        }

        /// <summary>
        /// Gets or sets the results for each succesful level.
        /// </summary>
        public TimeSpan[] Results
        {
            get { return this.results; }
            set { this.results = value; }
        }

        /// <summary>
        /// Gets or sets the jump movement multiplier of the daimyso character.
        /// </summary>
        public int DaimyoJumpticks
        {
            get { return this.daimyoJumpticks; }
            set { this.daimyoJumpticks = value; }
        }

        /// <summary>
        /// Gets gets or sets the model's repository.
        /// </summary>
        public IRepository GameRepository
        {
            get { return this.gameRepository; }
        }

        /// <inheritdoc/>
        public List<Character> CurrentEnemies
        {
            get { return this.currentEnemies; }
            set { this.currentEnemies = value; }
        }

        /// <inheritdoc/>
        public List<Projectile> Projectiles
        {
            get { return this.projectiles; }
            set { this.projectiles = value; }
        }

        /// <inheritdoc/>
        public ObservableCollection<Character> RemainingEnemies
        {
            get { return this.remainingEnemies; }
            set { this.remainingEnemies = value; }
        }

        /// <inheritdoc/>
        public Player ActualPlayer
        {
            get
            {
                return this.actualPlayer;
            }

            set
            {
                this.actualPlayer = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the model's platforms.
        /// </summary>
        public List<Platform> Platforms
        {
            get { return this.platforms; }
            set { this.platforms = value; }
        }

        /// <summary>
        /// Gets or sets the current level.
        /// </summary>
        public string CurrentLevel
        {
            get
            {
                return this.currentLevel;
            }

            set
            {
                this.currentLevel = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the game is finished or not.
        /// </summary>
        public bool GameFinsihed
        {
            get { return this.gameFinished; }
            set { this.gameFinished = value; }
        }

        /// <summary>
        /// Notifies when the property has changed.
        /// </summary>
        /// <param name="s">The property that has changed.</param>
        protected void OnPropertyChanged([CallerMemberName] string s = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(s));
        }
    }
}
