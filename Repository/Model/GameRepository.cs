﻿// <copyright file="GameRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using NinjaGame.Data;

    /// <summary>
    /// Contains the available gameobjects.
    /// </summary>
    public class GameRepository : IRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameRepository"/> class.
        /// </summary>
        public GameRepository()
        {
        }

        /// <inheritdoc/>
        public int ReadPlatformSpeed(string levelName)
        {
            return DataReader.ReadPlatformSpeed(levelName);
        }

        /// <inheritdoc/>
        public int ReadNumberOfEnemiesForLevel(string levelName)
        {
            return DataReader.ReadNumberOfEnemies(levelName);
        }

        /// <inheritdoc/>
        public int ReadNumberOfSamuraiForLevel(string levelName)
        {
            return DataReader.ReadNumberOfSamurais(levelName);
        }

        /// <inheritdoc/>
        public GameObject GetAGameObject(string gameObjectType)
        {
            return DataReader.GetAGameObject(gameObjectType);
        }
    }
}
