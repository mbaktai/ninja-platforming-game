﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Provides repository functionalities.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Represents the method for reading the number of enemies on a level.
        /// </summary>
        /// <param name="levelName">The level's name.</param>
        /// <returns>The amount of enemies on that level.</returns>
        int ReadNumberOfEnemiesForLevel(string levelName);

        /// <summary>
        /// Represents the method for reading the number of samurai on a level.
        /// </summary>
        /// <param name="levelName">The level's name.</param>
        /// <returns>The amount of samurai on that level.</returns>
        int ReadNumberOfSamuraiForLevel(string levelName);

        /// <summary>
        /// Represents the method for creating a gameobject.
        /// </summary>
        /// <param name="gameObjectType">The type of the gameobject.</param>
        /// <returns>A a game object of the specified type.</returns>
        NinjaGame.Data.GameObject GetAGameObject(string gameObjectType);

        /// <summary>
        /// Reads platform speed for the specified level.
        /// </summary>
        /// <param name="levelName">The level's name (e.g.: level1).</param>
        /// <returns>The speed of the platform.</returns>
        int ReadPlatformSpeed(string levelName);
    }
}
