﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace NinjaGame.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using NinjaGame.Data;

    /// <summary>
    /// Defines a game model (game state).
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets the actual player character.
        /// </summary>
        Player ActualPlayer { get; set; }

        /// <summary>
        /// Gets or sets the list of the currently visible enemies.
        /// </summary>
        List<Character> CurrentEnemies { get; set; }

        /// <summary>
        /// Gets or sets the list of the remaining enemies.
        /// </summary>
        ObservableCollection<Character> RemainingEnemies { get; set; }

        /// <summary>
        /// Gets or sets the current projectiles on the scree.
        /// </summary>
        List<Projectile> Projectiles { get; set; }
    }
}
